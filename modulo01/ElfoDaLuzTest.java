

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    
    @Test
    public void elfoDaLuzNasceComEspadaArcoFlecha(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        assertEquals("Arco", lumen.getInventario().obter(0).getDescricao());
        assertEquals("Flecha", lumen.getInventario().obter(1).getDescricao());
        assertEquals("Espada de Galvorn", lumen.getInventario().obter(2).getDescricao());
    }
    
    @Test
    public void elfoDaLuzNaoPerdeEspada(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        lumen.perderItem(new Item(1, "Espada de Galvorn"));
        assertEquals(new Item(1, "Espada de Galvorn"), lumen.getInventario().obter(2));
    }
    
    @Test
    public void elfoDaLuzPerde21VidaComUmAtaque(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        lumen.atacarComEspadaDeGalvorn(new Dwarf("Torin"));
        assertEquals(79.0, lumen.getVida(), 1e-9);
    }
    
    @Test
    public void elfoDaLuzGanha10VidaComDoisAtaques(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        lumen.atacarComEspadaDeGalvorn(new Dwarf("Torin"));
        lumen.atacarComEspadaDeGalvorn(new Dwarf("Torin"));
        assertEquals(89.0, lumen.getVida(), 1e-9);
    }
    
    @Test
    public void elfoDaLuzMorreApos17Ataques(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        for(int i=0; i < 18; i++){
            lumen.atacarComEspadaDeGalvorn(new Dwarf("Torin"));
        }
        assertEquals(0.0, lumen.getVida(), 1e-9);
        assertEquals(Status.MORTO, lumen.getStatus());
    }
    
    @Test
    public void elfoDaLuzNaoAtacaAposMorrer(){
        ElfoDaLuz lumen = new ElfoDaLuz("Lumen");
        for(int i=0; i < 19; i++){
            lumen.atacarComEspadaDeGalvorn(new Dwarf("Torin"));
        }
        assertEquals(0.0, lumen.getVida(), 1e-9);
        assertEquals(Status.MORTO, lumen.getStatus());
    }
    
    
}
