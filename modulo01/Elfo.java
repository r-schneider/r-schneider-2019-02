
public class Elfo extends Personagem implements AtacarDwarf{
    protected int indiceFlecha = 1;
    private static int qtdElfos;
    
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item (1, "Arco"));
        this.inventario.adicionar(new Item (4, "Flecha"));
        this.qtdExperienciaPorAtaque = 1;
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
        
    }
    
    public int getQuantidadeFlecha(){
        return this.getFlecha().getQuantidade();
        
    }
    
    public boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade() > 0;
    }
    
    public void atacarDwarfComArco(Dwarf anao){                
        if (podeAtirarFlecha()){
            this.getFlecha().setQuantidade(getQuantidadeFlecha() - 1);
            anao.receberDano();
            this.aumentarXP();
        }
    }
}
