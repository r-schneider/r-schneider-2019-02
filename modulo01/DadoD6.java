import java.util.*;

public class DadoD6 implements Sorteador{
    
    public int sortear(){
        Random gerador = new Random();
        return gerador.nextInt(6) + 1;
    }
}
