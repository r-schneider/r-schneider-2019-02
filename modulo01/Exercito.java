import java.util.*;

public class Exercito implements Estrategia{
   private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
    Arrays.asList(
        ElfoVerde.class,
        ElfoNoturno.class
    )
   );
   protected ArrayList<Elfo> exercito = new ArrayList<>();
   protected ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
   protected HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
   int posicao = 0;
   
   public Exercito(){
       this.exercito = new ArrayList<Elfo>();
   }
   
   public void alistarElfo(Elfo elfo){
       boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
       if(podeAlistar){
           exercito.add(elfo);
           ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
           if(elfoDoStatus == null){
               elfoDoStatus = new ArrayList<>();
               porStatus.put(elfo.getStatus(), elfoDoStatus);
           }
           elfoDoStatus.add(elfo);
        }
   }
   
   public ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> exercitoElfos){
      HashMap<Class, ArrayList<Elfo>> porClasse = new HashMap<>();
      for (Elfo elfo : exercitoElfos) {
          boolean estaVivo = elfo.getStatus() != Status.MORTO;
          if(estaVivo){
              ArrayList<Elfo> elfoDaClasse = porClasse.get(elfo.getClass());
              if (elfoDaClasse == null) {
                  elfoDaClasse = new ArrayList<>();
                  porClasse.put(elfo.getClass(), elfoDaClasse);
              }
              elfoDaClasse.add(elfo);
          }
      }
      elfosAtacantes.addAll(porClasse.get(ElfoVerde.class));
      elfosAtacantes.addAll(porClasse.get(ElfoNoturno.class));
      return elfosAtacantes;
   }
   
   public boolean verificaPosicao(){
       return this.posicao % 2 == 0;
   }
   
   public ArrayList<Elfo> ataqueAlternado(int tamanho, ArrayList<Elfo> elfosOrdenados){
      ArrayList<Elfo> elfosAlternados = new ArrayList<>(tamanho);
      int verde = 0;
      int noturno = elfosOrdenados.size() -1;
      for (int i = 0; i < tamanho; i++) {
          elfosAlternados.add(elfosOrdenados.get(verde));
          verde++;
          elfosAlternados.add(elfosOrdenados.get(noturno));
          noturno--;
      }
      
      return elfosAlternados;  
   }
   
   public int definirTamanhoExercito(ArrayList<Elfo> exercito){
       int elfoVerde = 0;
       int elfoNoturno = 0;
       
       for(Elfo elfo : exercito){
           if(elfo.getClass() == ElfoVerde.class){
           elfoVerde++;
           }
           else{
           elfoNoturno++;
           }
       }
       
       
       
       return elfoVerde > elfoNoturno ? elfoNoturno : elfoVerde;
   }
   
   public void executarAtaque(ArrayList<Elfo> exercitoElfos, Dwarf anao){
       for(Elfo elfo : exercitoElfos){
           boolean anaoEstaVivo = anao.getStatus() != Status.MORTO;
           if(anaoEstaVivo){
               elfo.atacarDwarfComArco(anao);
               
           }else{
               break;
           }
       }
   }
   
   public boolean verificarSeEstaVivoEComFlecha(Elfo elfo){
       return elfo.getStatus() != Status.MORTO &&  elfo.getQuantidadeFlecha() > 0;
   }
   
   public ArrayList<Elfo> elfosAptos(ArrayList<Elfo> exercito){
       ArrayList<Elfo> elfosAptos = new ArrayList<>();
       for(Elfo elfo: exercito){
           if(verificarSeEstaVivoEComFlecha(elfo)){
               elfosAptos.add(elfo);
           }
       }
       return elfosAptos;
   }
  
   public ArrayList<Elfo> ordernarElfosNoturnosPorPorcentagem(ArrayList<Elfo> exercito){
       ArrayList<Elfo> elfosOrdenados = new ArrayList<>();
       int contador = 0;
       double limiteD = elfosAptos(exercito).size() * 0.3;
       int limite = (int)limiteD;
       
       for(Elfo elfo : elfosAptos(exercito)){
           if(elfo.getClass() == ElfoVerde.class){
               elfosOrdenados.add(elfo);
           }
           else{
                if(contador < limite){
                    elfosOrdenados.add(elfo);
                    contador++;
                }
           }
       }
       return elfosOrdenados; 
   }
   
   public ArrayList<Elfo> getExercito(){
       return this.exercito;
   }
   
   public ArrayList<Elfo> buscar(Status status){
       return this.porStatus.get(status);
   }
   
}
