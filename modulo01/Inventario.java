import java.util.*;

public class Inventario{
   ArrayList<Item> inventario;
   
   public Inventario(int quantidade){
       this.inventario = new ArrayList<Item>(quantidade);
   }
   
   
   public ArrayList<Item> getInventario(){
       return this.inventario;
   }
   
   public void adicionar(Item novoItem){
       this.inventario.add(novoItem);
   }
   
   public Item obter(int posicao){
      if(posicao >= this.inventario.size()){
          return null;
      }
      return this.inventario.get(posicao);
   }
   
   public void remover(Item item){
       this.inventario.remove(item);
   }
   
   public String getDescricaoItens(){
       String descricaoItem = "";
       for(int k =0; k < this.inventario.size(); k++){
           if(inventario.get(k) != null){
               descricaoItem += "["+this.inventario.get(k).getDescricao()+"]";
           }
       }
       return descricaoItem;
   }
  
   //Resolução Método:
   // public String getDescricaoItens2(){
      // StringBuilder descricoes = new StringBuilder();
      // for(int k =0; k < this.itens.size(); k++){
          // Item item = this.itens.get(k);
          // if(item != null){
          // descricoes.append(item.getDescricao());
          // descricoes.append(",");
          // }
      // }
      // return (descricoes.length() >0 ? descricoes.substring(0, (descricoes.length()-1)): descricoes.toString());
   // }
   
   public Item itemMaiorQuantidade(){
       int auxIndex = 0;
       int auxQtd = 0;
       
       for(int l = 0; l < this.inventario.size(); l++){
           if(this.inventario.get(l)!=null){
               if(this.inventario.get(l).getQuantidade() > auxQtd){
                   auxQtd = this.inventario.get(l).getQuantidade();
                   auxIndex = l;
               }
           } 
       }
       return this.inventario.size() > 0 ? this.inventario.get(auxIndex) : null; //verifica se tem itens na lista antes de retornar
   }
   
   public Item buscar(String descricao){
       if(this.inventario.isEmpty()){
            return null;
        }
       
       int index = 0;
       for(int l = 0; l < inventario.size(); l++){
           if(this.obter(l).getDescricao().equals(descricao)){
               index = l;
           }
       }
       return this.inventario.get(index);
       
   }
   
   // Resolução método buscar()
   // public Item buscar(String descricao){
      // for(Item itemAtual : this.inventario){
          // boolean encontrei = itemAtual.getDescricao().equals(descricao);
          // if(encontrei){
              // return itemAtual;
          // }
      // }
   // }
   
   public ArrayList<Item> inverterLista(){
       ArrayList<Item> listaInvertida = new ArrayList<>(this.inventario.size());
       int auxIndex = 0;
       for(int l = this.inventario.size() -1; l >= 0; l--){
           listaInvertida.add(this.inventario.get(l));
       }
       return listaInvertida;
   }
   
   public void ordenarItens(){
       this.ordenarItens(TipoOrdenacao.ASC);
   }
  
   
   public void ordenarItens(TipoOrdenacao ordenacao){
       for(int i = 0; i < this.inventario.size(); i++){
           for(int j = 0; j < this.inventario.size() - 1; j++){
               Item atual = this.inventario.get(j);
               Item proximo = this.inventario.get(j + 1);
               boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade(): atual.getQuantidade() < proximo.getQuantidade();
               if(deveTrocar){
                   this.inventario.set(j, proximo);
                   this.inventario.set(j + 1, atual);
               }
           }
       }
   }
   
}
