

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest{
   // @Test
   // public void criarInventarioSemQuantidadeInformada(){
       // Inventario inventario = new Inventario();
       // assertEquals(99, inventario.getItens().length);
   // }
   
   // @Test
   // public void criarInventarioComQuantidadeInformada(){
       // Inventario inventario = new Inventario(37);
       // assertEquals(37, inventario.getItens().length);
   // }
   
   // @Test
   // public void adicionarUmItem(){
       // Inventario inventario = new Inventario();
       // Item espada = new Item(1, "Espada");
       // inventario.adicionar(espada);
       // assertEquals(espada, inventario.getItens()[0]);
   // }
   
   // @Test
   // public void adicionarDoisItens(){
       // Inventario inventario = new Inventario();
       // Item espada = new Item(1, "Espada");
       // Item pocao = new Item (3, "Poção");
       // inventario.adicionar(espada);
       // inventario.adicionar(pocao);
       // assertEquals(espada, inventario.getItens()[0]);
       // assertEquals(pocao, inventario.getItens()[1]);
   // }
   
   // @Test
   // public void removerItem(){
       // Inventario inventario = new Inventario();
       // Item adaga = new Item(2, "Adaga");
       // inventario.adicionar(adaga);
       // inventario.remover(0);
       // assertNull(inventario.obter(0));
   // }
   
   // @Test
   // public void buscarItemComMaiorQtd(){
       // Inventario inventario = new Inventario();
       // Item adaga = new Item(2, "Adaga");
       // Item pocao = new Item (3, "Poção");
       // Item espada = new Item (1, "Espada");
       // inventario.adicionar(adaga);
       // inventario.adicionar(pocao);
       // inventario.adicionar(espada);
       // assertEquals(pocao, inventario.itemMaiorQuantidade());
   // }
   
   // @Test
   // public void buscarItemComMaiorQtdVazio(){
       // Inventario inventario = new Inventario();
       // assertNull(inventario.itemMaiorQuantidade());
   // }
   
    // @Test
   // public void buscarItemComMaiorQtdItensMesmaQtd(){
       // Inventario inventario = new Inventario();
       // Item adaga = new Item(2, "Adaga");
       // Item pocao = new Item (2, "Poção");
       // Item espada = new Item (1, "Espada");
       // inventario.adicionar(adaga);
       // inventario.adicionar(pocao);
       // inventario.adicionar(espada);
       // assertEquals(adaga, inventario.itemMaiorQuantidade());
   // }
   
   // @Test
   // public void buscarDescricaoItens(){
       // Inventario inventario = new Inventario();
       // Item adaga = new Item(2, "Adaga");
       // Item pocao = new Item(3, "Poção");
       // Item espada = new Item(1, "Espada");
       // inventario.adicionar(adaga);
       // inventario.adicionar(pocao);
       // inventario.adicionar(espada);
       // assertEquals("[Adaga][Poção][Espada]", inventario.getDescricaoItens());
   // }
   
   // @Test
   // public void removerItemAntesDeAdicionarProximo(){
       // Inventario inventario = new Inventario(1);
       // Item adaga = new Item(2, "Adaga");
       // Item pocao = new Item(3, "Poção");
       // inventario.adicionar(adaga);
       // inventario.remover(0);
       // inventario.adicionar(pocao);
       // assertEquals(pocao, inventario.obter(0));
   // }
   
   //ARRAYLIST
   @Test
   public void buscarItemComInventarioVazio(){
       Inventario inventario = new Inventario(0);
       assertNull(inventario.buscar("Capa"));
   }    
   
   @Test
   public void CriarInventarioComTresItens(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(2, "Adaga");
        Item pocao = new Item(3, "Poção");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(adaga);
        inventario.adicionar(pocao);
        inventario.adicionar(espada);
        assertEquals(3, inventario.getInventario().size());
   }
   
   @Test
   public void adicionarItemAoInventario(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(2, "Adaga");
        inventario.adicionar(adaga);
        assertEquals(adaga, inventario.getInventario().get(0));
   }
   
   @Test
   public void buscarApenasUmItem(){
       Inventario inventario = new Inventario(1);
       Item adaga = new Item(2, "Adaga");
       inventario.adicionar(adaga);
       assertEquals(adaga, inventario.buscar("Adaga"));
   }
   
   @Test
   public void buscarItemPorDescricao(){
       Inventario inventario = new Inventario(5);
       Item adaga = new Item(2, "Adaga");
       Item pocao = new Item(3, "Poção");
       Item espada = new Item(1, "Espada");
       inventario.adicionar(adaga);
       inventario.adicionar(pocao);
       inventario.adicionar(espada);
       assertEquals(espada, inventario.buscar("Espada"));
   }
   
   @Test
   public void inverterInventarioVazio(){
       Inventario inventario = new Inventario(0);
       assertTrue(inventario.inverterLista().isEmpty());
   }
   
   @Test
   public void inverterComApenasUmItem(){
       Inventario inventario = new Inventario(5);
       Item adaga = new Item(2, "Adaga");
       inventario.adicionar(adaga);
       assertEquals(adaga, inventario.inverterLista().get(0));
       assertEquals(1, inventario.inverterLista().size());
   }    
   
   @Test
   public void inverterComDoisItens(){
       Inventario inventario = new Inventario(5);
       Item adaga = new Item(2, "Adaga");
       Item pocao = new Item(3, "Poção");
       inventario.adicionar(adaga);
       inventario.adicionar(pocao);
       assertEquals(pocao, inventario.inverterLista().get(0));
       assertEquals(2, inventario.inverterLista().size());
   }    

   // @Test
   // obteritemprimeiraposica
   
   // getdescricoesnenhumitem
   
   // getitemmaiorquantidadecomitenscommesmaquantidade
   
   // adicionardoisitens
}
