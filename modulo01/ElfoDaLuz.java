import java.util.*;

public class ElfoDaLuz extends Elfo{
    protected int qtdAtaqueComEspada;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
        "Espada de Galvorn" 
        )
   );
    
    {
        qtdAtaqueComEspada = 0;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21.0;
    }
    
    public boolean verificarAtaque(){
        return this.qtdAtaqueComEspada % 2 == 0;
    }
    
    public Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    
    public void atacarComEspadaDeGalvorn(Dwarf anao){
        if(verificarVida()){
            if(getEspada().getQuantidade() > 0){
                qtdAtaqueComEspada++;
                anao.receberDano();
                this.aumentarXP();
                if(verificarAtaque()){
                    this.vida += 10.0;
                }
                else{
                    this.vida -= this.vida >= this.qtdDano ? this.qtdDano : this.vida;
                }
            }    
        }
        if(!verificarVida()){
            this.status = Status.MORTO;
        }
    }
    
    @Override
    public void perderItem(Item item){
       boolean perder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(perder){
           super.perderItem(item);
       }       
    }
    
  
}
