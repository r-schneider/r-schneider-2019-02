
public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
    protected double qtdDanoPorAtaque;
    protected double qtdDano;

   {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        qtdExperienciaPorAtaque = 0;
        qtdDanoPorAtaque = 0;
        qtdDano = 0;
   }
    
   protected Personagem(String nome){
        this.nome = nome;
   }
    
   protected String getNome(){
        return this.nome;
   }
    
   protected Status getStatus(){
        return this.status;
   }
    
   protected Inventario getInventario(){
        return this.inventario;
   }
    
   protected double getVida(){
        return this.vida;
   }
    
   protected int getExperiencia(){
        return this.experiencia;
   }
    
   protected void setNome(String nome){
        this.nome = nome;
   }
    
   protected void ganharItem(Item item){
       this.inventario.adicionar(item);
   }
   
   protected void perderItem(Item item){
       this.inventario.remover(item);
   }
    
   protected boolean verificarVida(){
       return this.vida > 0;
   }
    
   protected void aumentarXP(){
       experiencia = experiencia + this.qtdExperienciaPorAtaque ;
   }
   
   protected double calcularDano(){
       return this.qtdDano;
   }
   
   protected void receberDano(){
        if(this.getStatus() == Status.ESCUDO_EQUIPADO){
            if(this.verificarVida()){
                this.vida = this.vida >= this.qtdDano ? this.vida -5.0 : this.vida;
                if(!this.verificarVida()){
                    this.status = Status.MORTO; 
                }
            } 
        }
        else{
            if(this.verificarVida()){
                this.vida = this.vida >= 10.0 ? this.vida -10.0 : this.vida;
                if(!this.verificarVida()){
                    this.status = Status.MORTO; 
                }
            }
        }
    }
   
   
    
}
