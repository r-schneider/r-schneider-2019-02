
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void criarUmExercitoEAdicionarElfo(){
        Exercito exercitoElfos = new Exercito();
        ElfoNoturno noturno = new ElfoNoturno("Dark");
        exercitoElfos.alistarElfo(noturno);
        assertEquals(noturno, exercitoElfos.exercito.get(0));
    }
    
    @Test
    public void adicionarUmElfoAoExercito(){
    }
    
    @Test
    public void elfosNoturnosPorUltimo(){
        Exercito exercitoElfos = new Exercito();
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Dark"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        elfosAtacantes = exercitoElfos.noturnosPorUltimo(exercitoElfos.getExercito());
        
        assertEquals(ElfoVerde.class, elfosAtacantes.get(0).getClass());
        assertEquals(ElfoVerde.class, elfosAtacantes.get(1).getClass());
        assertEquals(ElfoVerde.class, elfosAtacantes.get(2).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(3).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(4).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(5).getClass());
    }
    
    @Test
    public void elfosAtaqueAlternado(){
        Exercito exercitoElfos = new Exercito();
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Dark"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        int tamanho = exercitoElfos.definirTamanhoExercito(exercitoElfos.getExercito());
        elfosAtacantes = exercitoElfos.ataqueAlternado(tamanho, exercitoElfos.getExercito());
        
        assertEquals(ElfoVerde.class, elfosAtacantes.get(0).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(1).getClass());
        assertEquals(ElfoVerde.class, elfosAtacantes.get(2).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(3).getClass());
        assertEquals(ElfoVerde.class, elfosAtacantes.get(4).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(5).getClass());
    }
    
    @Test
    public void elfosAtaqueAlternadoNumeroImparDeElfos(){
        Exercito exercitoElfos = new Exercito();
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        int tamanho = exercitoElfos.definirTamanhoExercito(exercitoElfos.getExercito());
        elfosAtacantes = exercitoElfos.ataqueAlternado(tamanho, exercitoElfos.getExercito());
        
        assertEquals(ElfoVerde.class, elfosAtacantes.get(0).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(1).getClass());
        assertEquals(ElfoVerde.class, elfosAtacantes.get(2).getClass());
        assertEquals(ElfoNoturno.class, elfosAtacantes.get(3).getClass());

    }
    
    @Test
    public void definirTamanhoExercito(){
        Exercito exercitoElfos = new Exercito();
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        assertEquals(2, exercitoElfos.definirTamanhoExercito(exercitoElfos.getExercito()));  
       
    }
    
    @Test
    public void executarAtaqueAlternadoAnaoPerdeVida(){
        Exercito exercitoElfos = new Exercito();
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        Dwarf anaoAlvo = new Dwarf("Alvinho");
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        int tamanho = exercitoElfos.definirTamanhoExercito(exercitoElfos.getExercito());
        elfosAtacantes = exercitoElfos.ataqueAlternado(tamanho, exercitoElfos.getExercito());
        exercitoElfos.executarAtaque(elfosAtacantes, anaoAlvo);
        
        assertEquals(70.0, anaoAlvo.getVida(), 1e-9);
        assertEquals(4, elfosAtacantes.size());
        

    }
    
    @Test
    public void executarAtaqueNoturnosPorUltimoAnaoPerdeVida(){
        Exercito exercitoElfos = new Exercito();
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        Dwarf anaoAlvo = new Dwarf("Alvinho");
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        
        int tamanho = exercitoElfos.definirTamanhoExercito(exercitoElfos.getExercito());
        elfosAtacantes = exercitoElfos.noturnosPorUltimo(exercitoElfos.getExercito());
        exercitoElfos.executarAtaque(elfosAtacantes, anaoAlvo);
        
        assertEquals(60.0, anaoAlvo.getVida(), 1e-9);
        assertEquals(5, elfosAtacantes.size());
        

    }
    
    @Test
    public void ordenarElfosNoturnosPorPorcentagem(){
        Exercito exercitoElfos = new Exercito();
        
        exercitoElfos.alistarElfo(new ElfoVerde("Verdailen"));
        exercitoElfos.alistarElfo(new ElfoVerde("Greenarius"));
        exercitoElfos.alistarElfo(new ElfoVerde("Green"));
        exercitoElfos.alistarElfo(new ElfoVerde("Gren"));
        exercitoElfos.alistarElfo(new ElfoVerde("Gree"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Umbra"));
        exercitoElfos.alistarElfo(new ElfoNoturno("ailen"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Ubra"));
        exercitoElfos.alistarElfo(new ElfoNoturno("Nalen"));
        
        ArrayList<Elfo> noturnosPorPorcentagem = exercitoElfos.ordernarElfosNoturnosPorPorcentagem(exercitoElfos.getExercito());
        
        assertEquals("ailen", noturnosPorPorcentagem.get(7).getNome());
        
    }
    
}
