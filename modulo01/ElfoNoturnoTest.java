

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test
    public void elfoNoturnoGanhaExpX3AtirandoUmaFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        assertEquals(3, noturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoGanhaExpX3AtirandoDuasFlechas(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        assertEquals(6, noturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoPerde15VidaAtirandoFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        assertEquals(85, noturno.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNoturnoPerde30VidaAtirandoDuasFlechas(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        assertEquals(70, noturno.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNoturnoNaoAtiraFlechaSemVida(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.vida = 30;
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        assertEquals(0, noturno.getVida(), 1e-9);
        assertEquals(2, noturno.getQuantidadeFlecha());
    }
    
    @Test
    public void elfoNoturnoMorreAtirandoFlechas(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.vida = 30;
        Dwarf anao = new Dwarf("Target");
        noturno.atacarDwarfComArco(anao);
        noturno.atacarDwarfComArco(anao);
        assertEquals(Status.MORTO, noturno.getStatus());
    }
    
}
