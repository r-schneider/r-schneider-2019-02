
public class Dwarf extends Personagem{
        
   public Dwarf(String nome){
       super(nome);
       this.vida = 110.0;
       this.inventario.adicionar(new Item (1, "Escudo"));
        
   }
    
    public Status equiparEscudo(){
       for(int i = 0; i < inventario.getInventario().size(); i++){
           if(inventario.getInventario().get(0) == inventario.buscar("Escudo")){
               this.status = Status.ESCUDO_EQUIPADO;
           }
       }
       return this.status;
   }
   
   public double calcularDano(){
       return this.status == Status.ESCUDO_EQUIPADO ? 5.0 : this.qtdDano;
   }
}
