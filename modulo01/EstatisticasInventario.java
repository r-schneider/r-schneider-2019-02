
public class EstatisticasInventario{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){
        if(this.inventario.getInventario().isEmpty()){
            return Double.NaN;
        }
        
        double somaQtd = 0;
        for(Item item: inventario.getInventario()){
            somaQtd += item.getQuantidade();
        }
        return somaQtd / inventario.getInventario().size(); 
    }
    
    public double calcularMediana(){
        if(this.inventario.getInventario().isEmpty()){
            return Double.NaN;
        }
        
        int qtdItens = this.inventario.getInventario().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        if(qtdItens % 2 == 1){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter(meio-1).getQuantidade();
        return(qtdMeio + qtdMeioMenosUm) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia(){
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for(Item item : this.inventario.getInventario()){
            if(item.getQuantidade() > media){
                qtdAcima++;
            }
        }
        return qtdAcima;
    }
}
