
public class ElfoNoturno extends Elfo{
    
    public ElfoNoturno(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 3;
        this.qtdDanoPorAtaque = 15.0;
    }
    
    @Override
    public void atacarDwarfComArco(Dwarf anao){                
        if (podeAtirarFlecha()){
            if(verificarVida()){    
                this.vida -= this.vida >= this.qtdDanoPorAtaque ? this.qtdDanoPorAtaque : 0.0;
                this.getFlecha().setQuantidade(getQuantidadeFlecha() - 1);
                anao.receberDano();
                this.aumentarXP();
            }
            if(!verificarVida()){
                this.status = Status.MORTO;
            }
        }
    }
}
