import java.util.*;

public interface Estrategia
{
    ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> exercitoElfos);
    
    ArrayList<Elfo> ataqueAlternado(int tamanho, ArrayList<Elfo> elfosOrdenados);
}
