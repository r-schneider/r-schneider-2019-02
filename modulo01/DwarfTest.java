

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
    private final double DELTA = 1e-9;
    
    @Test
    public void criarDwarf(){
        Dwarf anao = new Dwarf("Torin");
        assertEquals("Torin",anao.getNome());
    }
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        assertEquals(110.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        novoAnao.receberDano();
        assertEquals(100.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 11; i++){
            novoAnao.receberDano();
        }
        assertEquals(00.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNasceComStatusRecemCriado(){
        Dwarf novoAnao = new Dwarf("Torin");
        assertEquals(Status.RECEM_CRIADO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfTemStatusAlteradoAoMorrer(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 12; i++){
            novoAnao.receberDano();
        }
        assertEquals(Status.MORTO, novoAnao.getStatus());
        assertEquals(0.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoTemStatusAlteradoSemPerderTodaVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 10; i++){
            novoAnao.receberDano();
        }
        assertEquals(Status.RECEM_CRIADO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfNaoFicaComVidaAbaixoDeZero(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 12; i++){
            novoAnao.receberDano();
        }
        assertEquals(00.0, novoAnao.getVida(), .000001);
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf anao = new Dwarf("Torin");
        assertEquals("Escudo",anao.getInventario().obter(0).getDescricao());
    }
    
   
    @Test
    public void dwarfEquipaEscudo(){
        Dwarf novoAnao = new Dwarf("Torin");
        Inventario inventario = new Inventario(1);
        novoAnao.equiparEscudo();
        assertEquals(Status.ESCUDO_EQUIPADO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfRecebeMetadeDoDanoComEscudoEquipado(){ //dwarfEquipaEscudoETomaMetadeDano
        Dwarf novoAnao = new Dwarf("Torin");
        
        novoAnao.equiparEscudo();
        for(int i = 0; i < 10; i++){
            novoAnao.receberDano();
        }
        assertEquals(60.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDano(){
        Dwarf novoAnao = new Dwarf("Torin");    
        for(int i=0; i<110; i++){
            novoAnao.receberDano();
        }
        
        assertEquals(00.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfMorreCom220DeDano(){
        Dwarf anao = new Dwarf("Torin");
        anao.equiparEscudo();
        for(int i = 0; i < 22; i++){
            anao.receberDano();
        }
        assertEquals(Status.MORTO, anao.getStatus());
        assertEquals(00.0, anao.getVida(), 1e-9);
    }

}
