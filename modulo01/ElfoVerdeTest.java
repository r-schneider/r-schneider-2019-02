

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    @Test
    public void elfoVerdeGanhaExpEmDobroAtirandoUmaFlecha(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Dwarf anao = new Dwarf("Target");
        elfoVerde.atacarDwarfComArco(anao);
        assertEquals(2, elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeGanhaExpEmDobroAtirandoDuasFlechas(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Dwarf anao = new Dwarf("Target");
        elfoVerde.atacarDwarfComArco(anao);
        elfoVerde.atacarDwarfComArco(anao);
        assertEquals(4, elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeGanhaItemDescrito(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Item espadaAcoValiriano = new Item(1, "Espada de aço valiriano");
        elfoVerde.ganharItem(espadaAcoValiriano);
        assertEquals(new Item(1, "Arco"), elfoVerde.getInventario().obter(0));
        assertEquals(new Item(4, "Flecha"), elfoVerde.getInventario().obter(1));
        assertEquals(espadaAcoValiriano, elfoVerde.getInventario().buscar("Espada de aço valiriano"));
    }
    
    @Test
    public void elfoVerdeNaoGanhaItemNaoDescrito(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Item espadaMadeira = new Item(1, "Espada de Madeira");
        elfoVerde.ganharItem(espadaMadeira);
        assertEquals(new Item(1, "Arco"), elfoVerde.getInventario().obter(0));
        assertEquals(new Item(4, "Flecha"), elfoVerde.getInventario().obter(1));
        assertNull(elfoVerde.getInventario().obter(2));
    }
    
    @Test
    public void elfoVerdePerdeItemDescrito(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Item espadaAcoValiriano = new Item(1, "Espada de aço valiriano");
        elfoVerde.ganharItem(espadaAcoValiriano);
        elfoVerde.perderItem(espadaAcoValiriano);
        assertNull(elfoVerde.getInventario().obter(2));
    }
    
    @Test
    public void elfoVerdeNaoPerdeItemNaoDescrito(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Item arco = new Item(1, "Arco");
        elfoVerde.perderItem(arco);
        assertEquals(arco, elfoVerde.getInventario().buscar("Arco"));
    }
}
