

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    private final double DELTA = 1e-9;
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atacarDwarfComArcoReduzirFlechaDarDanoAnao(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf ("Torin");
        elfo.atacarDwarfComArco(anao);
        assertEquals(100.0,anao.getVida(),DELTA);
        assertEquals(3,elfo.getQuantidadeFlecha());
    
    }
    
    @Test
    public void aumentarXP(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf ("Torin");
        elfo.atacarDwarfComArco(anao);
        assertEquals(1,elfo.getExperiencia(),DELTA);
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test
    public void criarDoisElfosIncrementaContadorDuasVezes(){
        new Elfo("Legolas");
        new Elfo("Lumen");
        assertEquals(2, Elfo.getQtdElfos());
    }
    
    @Test
    public void criarElfoNaoIncrementaContador(){
        assertEquals(0, Elfo.getQtdElfos());
    }
}
