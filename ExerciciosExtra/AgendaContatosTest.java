

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    
    @Test
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Teste", "5198765322");
        agenda.adicionar("Aeras", "5191425331");
        assertEquals("5191425331", agenda.consultar("Aeras"));
    } 
    
    @Test
    public void adicionarContatoEPesquisarPorTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Teste", "5198765322");
        agenda.adicionar("Aeras", "5191425331");
        assertEquals("Aeras", agenda.consultarNome("5191425331"));
    }
    
    @Test
    public void adicionarContatoEgerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Teste", "5198765322");
        agenda.adicionar("Aeras", "5191425331");
        String separador = System.lineSeparator();
        String esperado = String.format("Teste, 5198765322%sAeras, 5191425331%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}
