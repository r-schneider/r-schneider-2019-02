
public class TradutorParaIngles implements Tradutor{
    
    public String traduzir(String textoEmPt){
        switch(textoEmPt){
            case "Sim":
                return "Yes";
            case "Obrigado":
            case "Obrigada":
                return "Thank You";
            default:    
                return null;
        }        
    }
}
