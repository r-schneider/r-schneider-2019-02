class Pokemon{
    constructor(obj){
        this.pokeImg = obj.sprites.front_default
        this.idPokemon = obj.id
        this.nome = obj.name
        this.altura = obj.height
        this.peso = obj.weight
        this.tipo = obj.types
        this.status = obj.stats
    }

    get tiposMap(){
        return this.tipo.map(tipo => `<li>${tipo.type.name}</li>`).join(' ')
    }

    get statusMap(){
        return this.status.map(estatistica => `<li>${estatistica.stat.name}:  ${estatistica.base_stat}</li>`).join(' ')
    }
}