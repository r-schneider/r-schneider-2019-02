let pokeApi = new PokeApi();

function pokeBusca(){
    let previousId = document.querySelector('.idPokemon').innerHTML;
    let inputId = `ID: ${pokeForm.idPokemon.value}`    
    if(inputId != previousId){
        let pokemonEspecifico = pokeApi.buscarEspecifico(parseInt(pokeForm.idPokemon.value));
            pokemonEspecifico.then( pokemon =>{ 
                let poke = new Pokemon(pokemon);
                renderizacaoPokemon(poke);
            })
    }else{
        alert("Este pokémon já está sendo exibido na tela!")
    }
}

function renderizacaoPokemon( pokemon ){
    
    document.getElementById("pokeImg").src = pokemon.pokeImg;

    document.querySelector(".idPokemon").innerHTML = `ID: ${pokemon.idPokemon}`

    document.querySelector(".nome").innerHTML = pokemon.nome.toUpperCase();

    document.querySelector(".altura").innerHTML = `ALTURA: ${(pokemon.altura * 10)} Cm`

    document.querySelector(".peso").innerHTML = `PESO: ${(pokemon.peso / 10)} Kg`

    document.querySelector(".tipo").innerHTML = `TIPO(S): <ul>${pokemon.tiposMap}</ul>`

    document.querySelector(".status").innerHTML = `STATUS: <ul>${pokemon.statusMap}</ul>`
 
}

function estouComSorte(){
    let randomId = Math.floor(Math.random() * 802);
    let pokemonEspecifico = pokeApi.buscarEspecifico(randomId);
    pokemonEspecifico.then( pokemon =>{ 
        let poke = new Pokemon(pokemon);
        renderizacaoPokemon(poke);
    })
}

