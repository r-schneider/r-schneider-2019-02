//Exercício 01
function cardapioIFood( veggie = true, comLactose = false ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }
  if ( typeof cardapio.concat !== 'function' ) {
    cardapio = {}, cardapio.concat = () => []
  }

  cardapio =  [...cardapio,'pastel de carne', 'empada de legumes marabijosa' ]
  

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
    cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
  }
  
  let resultado = cardapio/*filter(alimento => alimento == 'cuca de uva')*/.map(alimento => alimento.toUpperCase());
  return resultado;
}

console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

function criarSanduiche(pao, recheio, queijo) {
    console.log(`Seu Sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)
}

const ingredientes = ['3 queijos','Frango','cheddar']
criarSanduiche(...ingredientes)

function receberValoresIndefinidos(...valores) {
    valores.map(valor => console.log(valor));
}

receberValoresIndefinidos([1, 3, 4, 5]);



let teste = { ..."Marcos"}

//transforma string em array
console.log([..."Marcos"])
//só separa
console.log(teste)


let inputTeste = document.getElementById('campoTeste')
console.log(inputTeste);
addEventListener('click', function(){
    console.log("chegou aqui");
})



String.prototype.correr = function(upper = false){
  let texto = `${this} estou correndo`;
  return upper ? texto.toUpperCase() : texto;
}

console.log("eu".correr());

