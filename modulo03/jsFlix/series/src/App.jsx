import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import './css/general.css';
import './css/header.css';
import './css/section.css';
import './css/form.css';
import './css/footer.css';
import './css/reactMirror.css';
import './css/jsFlix.css'
import Login from './Login';
import JsFlix from './JsFlix';
import ReactMirror from './ReactMirror';
import Header from './components/Header';
import Footer from './components/Footer';
import { PrivateRoute } from './components/PrivateRoute';
import ListaDeAvaliacoes from './components/ListaDeAvaliacoes'

class App extends Component {
  logout() {
    localStorage.removeItem( 'Authorization' );
  }

  render() {
    return (
      <div className="App">
      <button type="button" onClick={ this.logout.bind( this ) }>Logout</button>
        <Router>
          <Header/>
              <React.Fragment>
                  <PrivateRoute path="/" exact component={ PaginaInicial } />
                  <PrivateRoute path="/reactMirror/aval" exact component={ Aval } />
                  <Route path="/login" exact component={ Login } />
                  <PrivateRoute path="/jsFlix" component={ JsFlix } />
                  <PrivateRoute path="/reactMirror" component={ ReactMirror } />
              </React.Fragment>
          <Footer/>
        </Router>  
      </div>
    );
  }
}

const PaginaInicial = () => <section className="container row">
                              <h2>Página inicial</h2>
                            </section>

const Aval = () => <ListaDeAvaliacoes lista={this.avaliados} />

export default App;
