import React from 'react';

let series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')

Array.prototype.invalidas = function (){
    let invalidas = []
    for(let i = 0; i < this.length; i++){
        const isNull = Object.values(this[i]).some(atributo => (atributo === null));
        if(this[i].anoEstreia > 2019 || isNull ){    
            invalidas.push(this[i])
        }    
    }
    return invalidas.map(serie => <li>{serie.titulo}</li> )
}

Array.prototype.filtrarPorAno = function (ano){
    let seriesPorAno = []
    for(let i = 0; i < this.length; i++){
        if(this[i].anoEstreia >= ano){    
            seriesPorAno.push(this[i])
        }    
    }
    return `Séries a partir de ${ ano }: ${ seriesPorAno.map((serie) => <li>{serie.titulo}</li> ) }`
}

Array.prototype.procurarPorNome = function (nome){
    for(let i = 0; i < this.length; i++){
        for(let j = 0; j < this[i].elenco.length; j++){
            if(this[i].elenco[j].match(nome)){
                return true
            }
        }
    }
    return false
}

Array.prototype.mediaDeEpisodios = function (){
    let tamArray = parseInt(this.length)
    let nroEpisodios = 0
    for(let i = 0; i < this.length; i++){
        nroEpisodios += parseInt(this[i].numeroEpisodios)
    }
    return `${(nroEpisodios / tamArray)} Episódios`
}

Array.prototype.totalSalarios = function (indice){
    function imprimirBRL( valor ) {
        return parseFloat( valor.toFixed( 2 ) ).toLocaleString( 'pt-BR', {
        style: 'currency',
        currency: 'BRL'
        } )
    }
    let valorTotal = 0
    let salarioDiretores = (parseInt(this[indice].diretor.length) * 100000)
    let salarioAtores = (parseInt(this[indice].elenco.length) * 40000) 
    valorTotal = salarioDiretores + salarioAtores
    return imprimirBRL(parseFloat(valorTotal))
}

Array.prototype.queroGenero = function (generoSerie){
    let titulos =[]
    for(let i = 0; i < this.length; i++){
        if(Object.values(this[i].genero).some(atributo => (atributo === generoSerie))){
            titulos.push(this[i].titulo)
        }    
    }
    return `Séries de ${generoSerie}: ${ titulos.join( ' - ' ) }`   
}    

Array.prototype.queroTitulo = function (str){
    let titulos =[]
    for(let i = 0; i < this.length; i++){
        if(this[i].titulo.match(str)){
            titulos.push(this[i].titulo)
        }
    }
    return `Títulos aproximados: ${ titulos.join( ' - ' ) }`
}

Array.prototype.creditos = function (posicao){
    let creditos = []
    let titulo = this[posicao].titulo
    let elenco = sortCreditos(this[posicao].elenco)
    let diretor = sortCreditos(this[posicao].diretor)
    
    creditos.push(`Título: ${titulo} Elenco: ${elenco} Diretor(es): ${diretor}`)
    return creditos
    
}   

function splitPessoa(pessoa) {
    const pessoaSplit = pessoa.split(' ')
    let nomePessoa = {nome: pessoaSplit[0], sobrenome: pessoaSplit[1]}
    if(nomePessoa.sobrenome == null){
        nomePessoa.sobrenome = ' '
    }
    return nomePessoa
  }
  
function sortPessoas(p1, p2) {
    return p1.sobrenome.localeCompare(p2.sobrenome)
}
  
function listPessoas(pessoa) {
    return ` ${pessoa.nome} ${pessoa.sobrenome} `
}
  
function sortCreditos(array) {
    return array
      .map(splitPessoa)
      .sort(sortPessoas)
      .map(listPessoas)
}
  
export default series