import React, { Component } from 'react';
import axios from 'axios';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }
    
    trocaValoresState( event ) {
        const { name, value } = event.target;
        this.setState({ [name]: value })
    }

    logar( event ) {
        event.preventDefault();

        const { email, password } = this.state
        if( email && password ) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                password: this.state.password
            }).then( response => {
                localStorage.setItem('Authorization', response.data.token);
                this.props.history.push('/') 
                }
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                <h5>Logar</h5>
                <input className="field" type="text" id="nome" name="email" placeholder="Digite o username" onChange={ this.trocaValoresState.bind( this) }/><br/>
                <input className="field" type="text" id="password" name="password" placeholder="Digite o password" onChange={ this.trocaValoresState.bind( this) }/><br/>
                <button className="btn form-btn" type="button" onClick={ this.logar.bind( this ) }>Login</button>
            </React.Fragment>
        )
    }
}