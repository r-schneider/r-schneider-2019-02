import React from 'react'
import { Link } from 'react-router-dom';

const Header = props => {
    return (
        <React.Fragment>
            <header className="header">
                <nav className="container clearfix">
                    <h1 className="logo pull-left">React Mirror & JsFlix</h1>
                    <ul className="pull-right">
                        <li><Link to="/"><strong>Home</strong></Link></li>
                        <li><Link to="/JsFlix"><strong>JsFlix</strong></Link></li>
                        <li><Link to="/ReactMirror"><strong>ReactMirror</strong></Link></li>
                    </ul>
                </nav>
            </header>   
        </React.Fragment>    
    )
}

export default Header