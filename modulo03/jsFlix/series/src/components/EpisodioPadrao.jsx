import React from 'react'

const EpisodioPadrao = props => {
    const { episodio, exibirMensagem } = props
    return (
        <React.Fragment>
                <section className="container">
                    <div className="row clearfix">
                        <div className="col col-50 text-center pull-left">
                            <img className="mirror-img" src={ episodio.thumbUrl} alt={ episodio.nome}/>
                            <h4> Você já assistiu este episódio? { episodio.assistido ? 'Sim' : 'Não' }</h4>
                            <button className="btn mirror-btn" onClick={ props.marcarNoComp }>Assistido!</button>
                        </div>
                        <div className="col col-50 text-center pull-left">
                            <h1 className="mirror-h1">{ episodio.nome }</h1>
                            <h3>{ episodio.temporadaEpisodio }</h3>
                            <h3>{ episodio.duracaoEmMin }</h3>
                            <h3>Episódio assistido: { episodio.qtdVezesAssistido } vez(es)</h3>
                            <h3>Nota do episódio: { episodio.nota || 'Sem Nota'}</h3>
                            <h6 className="mirror-h6"/>
                            <button className="btn mirror-btn" onClick={ props.sortearNoComp }>Próximo</button>
                        </div>
                    </div>      
                </section>   
        </React.Fragment>    
    )
}

export default EpisodioPadrao
