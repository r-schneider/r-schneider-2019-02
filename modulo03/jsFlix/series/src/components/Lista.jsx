import React from 'react'

const Lista = (props) => {
    return (
        <React.Fragment>        
            <ul>
                <h2>{props.titulo}</h2>
                {props.function}
            </ul>
        </React.Fragment>
    );
}

export default Lista