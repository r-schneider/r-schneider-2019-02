import React from 'react'

const Form = (props) => {
    return (
        <React.Fragment>        
            <label className="label">{ props.label }</label> 
            <input  className="field" type="text" placeholder={ props.placeholder } name={ props.name } value={props.value} onChange={props.onChange}/>
            <button className="btn form-btn" onClick={ props.onClick }>Enviar</button>
        </React.Fragment>
    );
}

export default Form