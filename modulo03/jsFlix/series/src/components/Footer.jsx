import React from 'react'
import { Link } from 'react-router-dom';

const Footer = props => {
    return (
        <React.Fragment>
            <footer className="footer">
                <div className="container text-center clearfix">
                    <nav>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/JsFlix">JsFlix</Link></li>
                            <li><Link to="/ReactMirror">ReactMirror</Link></li>
                        </ul>
                    </nav>
                    <p>&copy;2019 React Mirror & jsFlix — Todos os direitos reservados</p>
                </div>
            </footer>     
        </React.Fragment>    
    )
}

export default Footer


           