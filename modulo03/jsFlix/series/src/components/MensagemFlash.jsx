import React, { Component } from 'react'
import PropTypes from 'prop-types';

const MensagemFlash = props => {

    return props.exibirMensagem ? (
        <React.Fragment>
           { props.notaValida ? msgVerde(): msgVermelha() }
        </React.Fragment>
    ):
    (
        ''
    )
    
    function msgVerde() {
        return(
            <div>
                <h3 className="msgVerde"> Nota registrada com sucesso!</h3>
            </div>
        )
    }
    function msgVermelha() {
        return(
            <div>
                <h3 className="msgVermelha"> Informar uma nota válida (entre 1 e 5) </h3>
            </div>
        )
    }    
 }
MensagemFlash.propTypes = {
    deveExibirMensagem: PropTypes.bool.isRequired,
    mensagem: PropTypes.string.isRequired,
    cor: PropTypes.oneOf(['verde', 'vermelho'])
}

MensagemFlash.defaultProps = {
     cor: 'verde',
     segundos: 3
}

export default MensagemFlash