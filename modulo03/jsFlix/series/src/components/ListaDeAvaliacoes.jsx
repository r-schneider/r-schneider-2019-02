import React, { Component } from 'react';

const ListaDeAvaliacoes = (props) => {
    return (
        <React.Fragment>
            <ul><h2>{props.lista.map(e => <li><h4>Título: {e.nome} / Nota: {e.nota}}</h4></li> )}</h2></ul>
        </React.Fragment>
    )
}

export default ListaDeAvaliacoes