import React, { Component } from 'react';
import * as axios from 'axios'; 
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao'
import MensagemFlash from './components/MensagemFlash'



class ReactMirror extends Component {
  constructor(props){
    super(props)
    this.listaEpisodios = new ListaEpisodios()
    this.avaliados = []
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      notaValida: false
    }
  }
  
  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state 
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  darNota(event){

    const { episodio } = this.state
    const nota = event.target.value
    if (nota > 0 && nota < 6) {
      episodio.avaliar(nota)
      this.setState({ notaValida: true })
      this.avaliados.push(episodio)
      console.log(this.avaliados)
      return this.avaliados
    }
    else {
      this.setState({ notaValida: false })
    }
    this.setState({
      episodio,
      exibirMensagem: true
    })
    setTimeout(() => { this.setState({ exibirMensagem: false })}, 5000)
  }

  gerarCampoNota(){
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <label>Dê uma nota de 1 a 5:</label><br/>
              <input type="text" placeholder="1 a 5" onBlur={ this.darNota.bind(this) }/>
            </div>
          )
        }
      </div>              
    )
  }

  render(){
    const { episodio, exibirMensagem, notaValida } = this.state
    return (
      <body>
        <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido }/>
        { this.gerarCampoNota() }
        <MensagemFlash exibirMensagem={ exibirMensagem } notaValida={ notaValida }/>
      </body>  
    );  
  }
}

export default ReactMirror;
