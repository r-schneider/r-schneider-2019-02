import React, { Component } from 'react';
import series from './models/series'
import Lista from './components/Lista';
import Form from './components/Form';

class JsFlix extends Component {
  constructor(props){
    super(props);
    this.state = { series: '', btn: '', btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false };
  }

  renderBtn(){
    if(this.state.btn == 'invalidas'){
      return(
      <Lista titulo="Séries Inválidas" function={series.invalidas()}/>
      )
    }
    if(this.state.btn == 'mediaEpisodios'){
      return(
      <Lista titulo="Média de Episódios" function={series.mediaDeEpisodios()}/>
      )
    }
  }

  btnInvalidas() {
    this.setState({ btn: 'invalidas'})
  }

  /* btnPorAno() {
    this.setState({ btn: })
  } */

  btnMediaEpisodios() {
    this.setState({ btn: 'mediaEpisodios'})
  }

  limpar() {
    this.setState({ series: '', btn: '', btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false })
  }

  handleChange (event) {
    this.setState({anoEstreia: event.target.value, nomePessoa: event.target.value, indiceSalarios: event.target.value, genero: event.target.value, palavraChave: event.target.value, creditos: event.target.value})
  }

  seriesInvalidas = () => {
    this.setState({ series: series.invalidas(), btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false })
  }
  
  btnPorAno() {
		this.setState({ series: '', btnPorAno: true, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false });
	}

  filtrarAno = (event) => {
    if(event) event.preventDefault();
    const ano = this.state.anoEstreia;
    this.setState({ series: series.filtrarPorAno(ano) })
  }

  gerarCampoAno() {
    return (
      <div> {
          this.state.btnPorAno && (
            <Form label="Filtrar séries por ano:" placeholder="Digite um ano" name="anoEstreia" value={ this.state.anoEstreia } onChange={ this.handleChange.bind(this) } onClick={ this.filtrarAno }/>
          )
      } </div>    
    )
  }

  btnPorNome() {
		this.setState({ series: '', btnPorAno: false, btnPorNome: true, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false });
	}

  buscarNome = (event) => {
    if(event) event.preventDefault();
    const nome = this.state.nomePessoa;
    let retorno = series.procurarPorNome(nome)
    if(retorno === true){
      this.setState({ series: `Este nome está presente em um dos elencos.` })
    }else{
      this.setState({ series: `Este nome não faz parte de elenco algum.` })
    }
  }

  gerarCampoNome() {
    return(
      <div>
        {
          this.state.btnPorNome && (
            <div>
                <label className="label" >Buscar ator ou atriz nos elencos:</label> 
                <input  className="field" type="text" placeholder="Digite um nome" name="nomePessoa" value={this.state.nomePessoa} onChange={this.handleChange.bind(this)}/>
                <button className="btn form-btn" onClick={ this.buscarNome }>Enviar</button>
            </div>
          )
        }
      </div>    
    )
  }

  mediaEpisodios = () => {
    this.setState({ series: series.mediaDeEpisodios(), btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false })
  }

  btnSalarios() {
		this.setState({ series: '', btnPorAno: false, btnPorNome: false, btnSalarios: true, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: false });
  }
  
  valorGasto = (event) => {
    if(event) event.preventDefault();
    const indiceSalarios = this.state.indiceSalarios;
    this.setState({ series: series.totalSalarios(indiceSalarios) })
  }

  gerarCampoSalarios() {
    return(
      <div>
        {
          this.state.btnSalarios && (
            <div>
                <label className="label" >Verificar valor gasto em salários:</label> 
                <input  className="field" type="text" placeholder="Digite o índice de uma série" name="indiceSalarios" value={this.state.indiceSalarios} onChange={this.handleChange.bind(this)}/>
                <button className="btn form-btn" onClick={ this.valorGasto }>Enviar</button>
            </div>
          )
        }
      </div>    
    )
  }

  btnPorGenero() {
		this.setState({ series: '', btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: true, btnPorPalavraChave: false, btnCreditos: false });
  }

  seriePorGenero = (event) => {
    if(event) event.preventDefault();
    const genero = this.state.genero;
    this.setState({ series: series.queroGenero(genero) })
  }

  gerarCampoGenero(){
    return(
      <div>
        {
          this.state.btnPorGenero && (
            <div>
                <label className="label" >Buscar série por gênero:</label> 
                <input  className="field" type="text" placeholder="Digite um gênero" name="genero" value={this.state.genero} onChange={this.handleChange.bind(this)}/>
                <button className="btn form-btn" onClick={ this.seriePorGenero }>Enviar</button>
            </div>
          )
        }
      </div>    
    )
  }

  btnPorPalavraChave() {
		this.setState({ series: '', btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: true, btnCreditos: false });
  }

  seriePorPalavraChave = (event) => {
    if(event) event.preventDefault();
    const palavraChave = this.state.palavraChave;
    this.setState({ series: series.queroTitulo(palavraChave) })
  }

  gerarCampoTitulo() {
    return(
      <div>
        {
          this.state.btnPorPalavraChave && (
            <div>
                <label className="label" >Buscar série por título:</label> 
                <input  className="field" type="text" placeholder="Digite uma palavra chave" name="palavraChave" value={this.state.palavraChave} onChange={this.handleChange.bind(this)}/>
                <button className="btn form-btn" onClick={ this.seriePorPalavraChave }>Enviar</button>
            </div>
          )
        }
      </div>    
    )
  }

  btnCreditos() {
		this.setState({ series: '', btnPorAno: false, btnPorNome: false, btnSalarios: false, btnPorGenero: false, btnPorPalavraChave: false, btnCreditos: true });
  }

  serieCreditos = (event) => {
    if(event) event.preventDefault();
    const indice = this.state.creditos;
    this.setState({ series: series.creditos(indice) })
  }

  gerarCampoCreditos() {
    return(
      <div>
        {
          this.state.btnCreditos && (
            <div>
                <label className="label" >Visualizar créditos de uma série:</label>
                <input  className="field" type="text" placeholder="Digite um índice" name="creditos" value={this.state.creditos} onChange={this.handleChange.bind(this)}/>
                <button className="btn form-btn" onClick={ this.serieCreditos }>Enviar</button>
            </div>
          )
        }
      </div>    
    )
  }

  render() {
    return (
      <div className="container">
        <section className="row clearfix">
          <div className="col col-40 pull-left">
            <button className="btn jsflix-btn" onClick="#">Mostrar Títulos</button>
            <button className="btn jsflix-btn" onClick={ this.btnInvalidas.bind(this) }>Séries Inválidas</button>
            <button className="btn jsflix-btn" onClick={ this.btnPorAno.bind(this) }>Filtrar por Ano</button>
            <button className="btn jsflix-btn" onClick={ this.btnPorNome.bind(this) }>Buscar Ator/Atriz</button>
            <button className="btn jsflix-btn" onClick={ this.btnMediaEpisodios.bind(this) }>Média de Episódios</button>
            <button className="btn jsflix-btn" onClick={ this.btnSalarios.bind(this) }>Gasto com Salários</button>
            <button className="btn jsflix-btn" onClick={ this.btnPorGenero.bind(this) }>Buscar Gênero</button>
            <button className="btn jsflix-btn" onClick={ this.btnPorPalavraChave.bind(this) }>Buscar Título</button>
            <button className="btn jsflix-btn" onClick={ this.btnCreditos.bind(this) }>Créditos</button>
            <button className="btn jsflix-btn" onClick={ this.limpar.bind(this) }>Limpar</button>
          </div>
          <div className="col col-60 pull-left text-center">
            <form className="form" >
            { this.gerarCampoAno() }
            { this.gerarCampoNome() }
            { this.gerarCampoSalarios() }
            { this.gerarCampoGenero() }
            { this.gerarCampoTitulo() }
            { this.gerarCampoCreditos() }
            </form>
            <h2>{ this.state.series }</h2>
            <div>{this.renderBtn()}</div>

            {/* definir estado botao, alterar estado, if estado imprime tal resultado, necessario somente os set states ... 
            Criar componentes para botões, forms e exibição do return das funções*/}
          </div>
        </section>  
      </div>
    );
  }
}
export default JsFlix;
