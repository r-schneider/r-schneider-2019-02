import { Component } from 'react';
import axios from 'axios';


export default class BancoApi extends Component {

    async listarAgencias () {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = 'http://localhost:1337/agencias'
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.agencias
        return retorno
    }

    async agenciaPorId ( id ) {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = `http://localhost:1337/agencia/${id}`
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.agencias
        return retorno
    }

    async listarClientes () {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = 'http://localhost:1337/clientes'
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.clientes
        return retorno
    }

    async ClientePorId ( id ) {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = `http://localhost:1337/cliente/${id}`
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.clientes
        return retorno
    }

    async listarTipoContas () {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = 'http://localhost:1337/tipoContas'
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.tipos
        return retorno
    }

    async TipoContaPorId ( id ) {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = `http://localhost:1337/tipoConta/${id}`
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.tipos
        return retorno
    }

    async listarContaClientes () {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = 'http://localhost:1337/conta/clientes'
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.cliente_x_conta
        console.log(retorno)
        return retorno
        
    }

    async ContaClientePorId ( id ) {
        const AuthStr = 'banco-vemser-api-fake'
        const URL = `http://localhost:1337/conta/cliente/${id}`
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data.cliente_x_conta
        return retorno
    }
}