import React, { Component } from 'react';
import Button from '../components/button/Button'
import BancoApi from '../banco-api/BancoApi'

import Lista from '../components/lista/Lista'
import '../components/card/card.css';
import Input from '../components/input/Input';

export default class User extends Component {
    constructor( props ) {
        super( props )
        this.banco = new BancoApi()
        this.state = {
            btn: '',
            btnInfo: '',
            render: [],
            validador: '',
            info: {},
        }
    }

    marcarDigital( event ) {
        const ag = event
        ag.isDigital = !ag.isDigital
    }

    buscarDigital() {
        const isDigital = this.state.render.filter(ag => ag.isDigital === true)
        this.setState({
            render: isDigital
        })
    }

    limparCampo() {
        this.setState({ info: '', validador: '' })
    }

    async getAgencias () {
        const render = await this.banco.listarAgencias()
        this.setState({
            btn: true,
            btnInfo: '',
            validador: 'agencias',
            render
        })
    }

    async getClientes () {
        const render = await this.banco.listarClientes()
        this.setState({
            btn: true,
            btnInfo: '',
            validador: 'clientes',
            render
        })
    }

    async getTipoContas () {
        const render = await this.banco.listarTipoContas()
        this.setState({
            btn: true,
            btnInfo: '',
            validador: 'tipoContas',
            render
        })
    }

    async getClienteContas () {
        const render = await this.banco.listarContaClientes()
        this.setState({
            btn: true,
            btnInfo: '',
            validador: 'clienteContas',
            render
        })
    }

    renderAgencias() {
        const agenciasMap  = this.state.render.map(( ag ) => ( <li key={ ag.id }>Id: {ag.id} - Agência: { ag.nome } <br/> <Button class="info-btn" text="+ info" onClick={ this.getInfo(ag) }/><label className="digital"><input type="checkbox" onClick={ () => this.marcarDigital(ag) }/>Digital</label><hr/></li> ))
        return(
            <div>
            {
                this.state.btn && (
                    <div className="card-info clearfix">
                        <Input label="Buscar agência:" placeholder="Digite o nome de uma agência" onChange={ this.filtrarAgenciaPorNome.bind(this) }/>
                        <Lista title="Agências" lista={ agenciasMap }/>
                        
                        <Button text="Agências Digitais" class="search-btn" onClick={ this.buscarDigital.bind(this) }/>
                    </div>
                )
            }
            </div>    
        )
    }

    renderClientes() {
        const clientesMap  = this.state.render.map(( cliente ) => ( <li key={ cliente.id }>Id: { cliente.id } <br/> { cliente.nome } <br/> <Button class="info-btn" text="+ info" onClick={ this.getInfo(cliente) }/><hr/></li> ))
        return(
            <div>
            {
                this.state.btn && (
                    <div className="card-info">
                        <Input label="Buscar cliente:" placeholder="Digite o nome de um cliente" onChange={ this.filtrarClientePorNome.bind(this) }/>
                        <Lista title="Clientes" lista={ clientesMap }/>
                    </div>
                )
            }
            </div>    
        )
    }

    renderTipoContas() {
        const tipoContasMap  = this.state.render.map(( tc ) => ( <li key={ tc.id }>Id: {tc.id} - Tipo: { tc.nome }<hr/></li> ))
        return(
            <div>
            {
                this.state.btn && (
                    <div className="card-info">
                        <Input label="Buscar tipo de conta:" placeholder="Digite um tipo de conta" onChange={ this.filtrarContaPorTipo.bind(this) }/>
                        <Lista title="Tipos de Conta" lista={ tipoContasMap }/>
                    </div>
                )
            }
            </div>    
        )
    }

    renderClienteContas() {
        const clienteContasMap  = this.state.render.map(( cc ) => ( <li key={ cc.id }>Código da Agência: {cc.codigo} <br/> Tipo de conta: { cc.tipo.nome } <br/> Cliente: { cc.cliente.nome } <br/> <Button class="info-btn" text="+ info" onClick={ this.getInfo(cc) }/><hr/></li> ))
        return(
            <div>
            {
                this.state.btn && (
                    <div className="card-info">
                        <Input label="Buscar cliente x conta:" placeholder="Digite um cliente" onChange={ this.filtrarClienteConta.bind(this) }/>
                        <Lista title="Cliente X Conta" lista={ clienteContasMap }/>
                    </div>
                )
            }
            </div>    
        )
    }

    renderInfoAgencia() {
        const ag = this.state.info
        return(
            <div>
                {
                this.state.btnInfo && (
                    <div className="card-info">
                        <ul>
                            <h3>Informações da agência:</h3><br/>
                            <li>
                                <h5> 
                                    Endereço: { ag.endereco.logradouro }, { ag.endereco.numero} <br/> 
                                    Bairro: { ag.endereco.bairro } <br/>
                                    Cidade: { ag.endereco.cidade } - { ag.endereco.uf } 
                                </h5>
                                <Button text="Voltar" class="clear-btn" onClick={ this.limparCampo.bind(this)}/>
                            </li>
                        </ul>
                    </div>    
                )
                }
            </div>       
        ) 
    }

    renderInfoCliente() {
        const cliente = this.state.info
        return(
            <div>
                {
                this.state.btnInfo && (
                    <div className="card-info">
                        <ul>
                            <h3>Informações do Cliente:</h3><br/>
                            <li>
                                <h5> 
                                    Nome: { cliente.nome } <br/>
                                    CPF: { cliente.cpf } <br/> 
                                    Agência: { cliente.agencia.nome } - Cod: { cliente.agencia.codigo } <br/>
                                    Endereço: { cliente.agencia.endereco.logradouro }, { cliente.agencia.endereco.numero } <br/>
                                    { cliente.agencia.endereco.bairro } - { cliente.agencia.endereco.cidade } / { cliente.agencia.endereco.uf }
                                </h5>
                                <Button text="Voltar" class="clear-btn" onClick={ this.limparCampo.bind(this)}/>
                            </li>
                        </ul>
                    </div>    
                )
                }
            </div>       
        ) 
    }

    renderInfoClienteContas() {
        const clienteConta = this.state.info
        return(
            <div>
                {
                this.state.btnInfo && (
                    <div className="card-info">
                        <ul>
                            <h3>Informações Cliente x Conta:</h3><br/>
                            <li><h5> Id: { clienteConta.cliente.id } <br/>
                                     Nome: { clienteConta.cliente.nome } <br/> 
                                     CPF: { clienteConta.cliente.cpf } <br/>
                                     Id Agência: { clienteConta.id } <br/>
                                     Cod Agência: { clienteConta.codigo } <br/>
                                     Tipo de Conta: { clienteConta.tipo.nome }
                                </h5>
                                <Button text="Voltar" class="clear-btn" onClick={ this.limparCampo.bind(this)}/>
                            </li>
                        </ul>
                    </div>    
                )
                }
            </div>       
        ) 
    }

    filtrarAgenciaPorNome( event ) {
        event.preventDefault()
        const render = this.banco.listarAgencias()
        this.setState({
            render
        })
        const filtro = this.state.render.filter(ag => ag.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            render: filtro
        })
    }
    
    filtrarClientePorNome( event ) {
        event.preventDefault()
        const render = this.banco.listarClientes()
        this.setState({
            render,
        })
        const filtro = this.state.render.filter(cliente => cliente.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            render: filtro
        })
    }
    
    filtrarContaPorTipo( event ) {
        event.preventDefault()
        const render = this.banco.listarTipoContas()
        this.setState({
            render
        })
        const filtro = this.state.render.filter(tipo => tipo.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            render: filtro
        })
    } 

    filtrarClienteConta( event ) {
        event.preventDefault()
        const render = this.banco.listarContaClientes()
        this.setState({
            render
        })
        const filtro = this.state.render.filter(conta => conta.cliente.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            render: filtro
        })
    } 

    renderLista = () => {
        if( this.state.validador === 'agencias' ){
            return this.renderAgencias()
        }
        if( this.state.validador === 'clientes' ){
            return this.renderClientes()
        }
        if( this.state.validador === 'tipoContas' ){
            return this.renderTipoContas()
        }
        if( this.state.validador === 'clienteContas'){
            return this.renderClienteContas()
        }
    }

    getInfo = value => event => {
        event.preventDefault()

        this.setState({ btnInfo: true, info: value })
    }

    renderInfo = ( ) => {
        if(this.state.validador === 'agencias'){
            return this.renderInfoAgencia()
        }
        if(this.state.validador === 'clientes'){
            return this.renderInfoCliente()
        }
        if(this.state.validador === 'clienteContas'){
            return this.renderInfoClienteContas()
        }
    }

    render() {
        return (
            <div>
                <div className="container btn-navbar clearfix">
                    <Button class="form-btn bar-btn" onClick={ this.getAgencias.bind(this) } text="Agências"/>
                    <Button class="form-btn bar-btn" onClick={ this.getClientes.bind(this) } text="Clientes"/>
                    <Button class="form-btn bar-btn" onClick={ this.getTipoContas.bind(this) } text="Tipos de Conta"/>
                    <Button class="form-btn bar-btn" onClick={ this.getClienteContas.bind(this) } text="Cliente x Conta"/>
                </div>
                <div className="container">
                    <div className="row clearfix">
                        <div className="col col-50 pull-left">
                            { this.renderLista() }
                        </div>
                        <div className="col col-50 pull-left">
                            { this.renderInfo() }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}