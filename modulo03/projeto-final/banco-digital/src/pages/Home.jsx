import React, { Component } from 'react';
import Card from '../components/card/Card';

export default class Home extends Component {
    
    render() {
        return (
            <div>
            <div className="banner">
                <div className="container">
                    <div className="table">
                        <div className="table-cell text-center">
                            <h1><strong>Completo, digital e gratuito</strong></h1>
                            <h3><strong>Baixe o App e aproveite sua conta sem tarifas.</strong></h3> 
                        </div>
                    </div>
                </div>
            </div>
            <div className="container text-center">
                <div className="row clearfix">
                    <div className="col col-50 pull-left">
                        <Card titulo="Cartão de Crédito" artigo="Moderno, simples e gratuito. Tudo para deixar o controle nas suas mãos."/>
                    </div>
                    <div className="col col-50 pull-left">
                        <Card titulo="Digiconta" artigo="Rende mais que poupança e você não paga nada por isso."/> 
                    </div>
                </div>
            </div>
        </div>
        )
    }
}