import React, { Component } from 'react';
import axios from 'axios';

import Input from '../components/input/Input';
import Button from '../components/button/Button';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            senha: ''
        }
    }
    
    trocaValoresState( event ) {
        const { name, value } = event.target;
        this.setState({ [name]: value })
    }

    logar( event ) {
        event.preventDefault();

        const { email, senha } = this.state
        if( email && senha ) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then( response => {
                localStorage.setItem('Authorization', response.data.token);
                this.props.history.push('/user') 
                }
            )
        }
    }

    logout() {
        localStorage.removeItem( 'Authorization' );
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row text-center">
                        <div className="col card-login col-login">
                            <Input label="E-mail" id="email" name="email" placeholder="email@exemplo.com.br" onChange={ this.trocaValoresState.bind( this) }/>
                            <Input label="Senha"id="senha" name="senha" placeholder="Digite sua senha" onChange={ this.trocaValoresState.bind( this) }/>
                            <Button class="form-btn" onClick={ this.logar.bind(this) } text="Entrar"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}