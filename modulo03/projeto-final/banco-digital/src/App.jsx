import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './components/private-route/PrivateRoute';

import Home from './pages/Home';
import Login from './pages/Login';
import User from './pages/User';

import Header from './components/header/Header';
import Footer from './components/footer/Footer';

import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
          <Header path1="/" link1="Home" path2="/user" link2="Minha conta" class3="header-btn" path3="/login" link3="Login"/> 
            <React.Fragment>
                <PrivateRoute path="/" exact component={ Home } />
                <Route path="/login" exact component={ Login } />
                <PrivateRoute path="/user" exact component={ User } />
            </React.Fragment>
          <Footer/>
        </Router>  
      </div>
    );
  }
}

export default App;
