import React from 'react'
import { Link } from 'react-router-dom';
import './header.css'
import '../button/button.css'

const Header = props => {
    return (
        <React.Fragment>
            <header className="header">
                <nav className="container clearfix">
                    <h1 className="logo pull-left">Digibank</h1>
                    <ul className="pull-right">
                        <li><Link className={ props.class1 } to={ props.path1 }>{ props.link1 }</Link></li>
                        <li><Link className={ props.class2 } to={ props.path2 }>{ props.link2 }</Link></li>
                        <li ><Link className={ props.class3 } to={ props.path3 }>{ props.link3 }</Link></li>
                    </ul>
                </nav>
            </header>   
        </React.Fragment>    
    )
}

export default Header