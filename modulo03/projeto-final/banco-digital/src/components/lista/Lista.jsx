import React from 'react'

const Lista = ( props ) => {
    return (
        <React.Fragment>        
            <ul>
                <h2>{ props.title }</h2>
                <h5>{ props.lista }</h5>
            </ul>
        </React.Fragment>
    );
}

export default Lista