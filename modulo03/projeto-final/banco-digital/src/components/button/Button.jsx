import React from 'react'
import './button.css';

const Button = (props) => {
    return (
        <React.Fragment>        
            <button className={ `btn ${ props.class } ` } type="button" onClick={ props.onClick }>{ props.text }</button>
        </React.Fragment>
    );
}

export default Button