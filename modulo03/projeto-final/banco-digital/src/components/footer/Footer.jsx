import React from 'react'
import { Link } from 'react-router-dom';
import './footer.css'

const Footer = props => {
    return (
        <React.Fragment>
            <footer className="footer">
                <div className="container text-center clearfix">
                    <nav>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="#">Quem somos</Link></li>
                            <li><Link to="#">Contato</Link></li>
                        </ul>
                    </nav>
                    <p>&copy;2019 Digibank — Todos os direitos reservados</p>
                </div>
            </footer>     
        </React.Fragment>    
    )
}

export default Footer