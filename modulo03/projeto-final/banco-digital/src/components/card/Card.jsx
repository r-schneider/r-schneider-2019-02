import React from 'react'
import Button from '../button/Button';
import './card.css';

const Card = (props) => {
    return (
        <React.Fragment>
            <section className="card">
                <article>        
                    <h2>{ props.titulo }</h2><br/> 
                    <h4>{ props.artigo }</h4>
                </article>
                <Button class="form-btn"text="Saiba Mais"/>
            </section>
        </React.Fragment>
    );
}

export default Card