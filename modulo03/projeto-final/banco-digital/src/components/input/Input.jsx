import React from 'react'
import './input.css';

const Input = (props) => {
    return (
        <React.Fragment>
            <div>        
                <label className="label">{ props.label }</label><br/> 
                <input  className="field" type="text" id={ props.id } name={ props.name } placeholder={ props.placeholder } onChange={props.onChange} value={props.value}/>
            </div>
        </React.Fragment>
    );
}

export default Input