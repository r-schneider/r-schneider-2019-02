//Exercício 01
function arredondar(numero, precisao = 2){
    const fator = Math.pow(10, precisao)
    return Math.ceil(numero * fator) / fator
}

function imprimirGBP(valor){
    let qtdMilhares = 3;
    let separadorMilhar = ","
    let separadorDecimal = "."

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(valor)%1)
    let parteInteira = Math.trunc(valor)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if (c % qtdMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        }else if (parteInteiraString.length < qtdMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '£' : '-£'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;
}


console.log(imprimirGBP(0));
console.log(imprimirGBP(3498.99));
console.log(imprimirGBP(-3498.99));
console.log(imprimirGBP(2313477.0135));

//Exercício 02

function imprimirFR(valor){
    let qtdMilhares = 3;
    let separadorMilhar = ","
    let separadorDecimal = "."

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(valor)%1)
    let parteInteira = Math.trunc(valor)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if (c % qtdMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        }else if (parteInteiraString.length < qtdMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '' : '-'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}€`;
}


console.log(imprimirFR(0));
console.log(imprimirFR(3498.99));
console.log(imprimirFR(-3498.99));
console.log(imprimirFR(2313477.0135));
