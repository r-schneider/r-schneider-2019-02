console.log("Cheguei");

var teste = "123"; //possível atribuir duas vezes
let teste2 = "123"; // somente pode atribuir uma vez e alterar o valor
const teste3 = 123; // valor não pode ser alterado, é constante

var teste = "111";
teste2 = "222";

console.log(teste2);

{
    let teste2 = "Aqui mudou"
    console.log(teste2);
}

console.log(teste2);

const pessoa = {
    nome: "John",
    idade: 29,
    endereco: {
        logradouro: "Rua da Esquina",
        numero: 123
    }
}

Object.freeze(pessoa);

pessoa.nome = "John Doe";

console.log(pessoa);


function somar(valor1, valor2){
    console.log(valor1 + valor2);
}
somar(3, 5);

function ondeMoro(cidade){
    console.log("Eu moro em " + cidade + " e sou muito feliz.");
    console.log(`Eu moro em ${cidade} e sou muito feliz.`); //com crase
}

ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                +"\n"
                +"Maçã"
                +"\n"
                +"Laranja"
                +"\n"
                +"Limão";
    console.log(texto);
    let newTexto = `Banana
Maçã
Laranja
Limão`
    console.log(newTexto);
}

fruteira();

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos`);
}

quemSou(pessoa);

let funcaoSomarVar = function(a, b, c = 0){
    return a + b + c;
}

let add = funcaoSomarVar 
let resultado = add(3,2)


console.log(resultado);

const {nome:n, idade:i} = pessoa
const {endereco: {logradouro, numero}} = pessoa
console.log(logradouro, numero);

const array = [1, 3, 4, 8];
const [n1, n2, n3 = 9] = array
console.log(n1, n2, n3);

function testarPessoa({nome, idade, endereco:{logradouro}}){
    console.log(nome, idade, logradouro);
}

testarPessoa(pessoa);

let a1 = 42;
let b1 = 15;

console.log (a1, b1);

[a1, b1] = [b1, a1];

console.log (a1, b1);