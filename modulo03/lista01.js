//Exercício 01

function calcularCirculo(raio, tipoCalculo){
        if(tipoCalculo == "a"){
            return Math.ceil(Math.PI* (raio * raio));
         } 
        if(tipoCalculo == "c"){
             return Math.ceil((2 * raio) * Math.PI);
        }
}

console.log(calcularCirculo(2, "a"));


//Exercício 02

function naoBissexto(ano){
    if((ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0)){
        return false;
    }
    return true;
}

//arrow function let naoBissexto = ano => (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true; 

console.log(naoBissexto(2012));
console.log(naoBissexto(1998));

//Exercicio 03

function somarPares(array = []){
    let soma = 0;
    for (let i = 0; i < array.length; i++){
        if(i % 2 == 0){
            soma += array[i];
        }
    }
    return soma;
}

resultado4 = somarPares([1, 3, 4, 8, 9, 21]);
console.log(resultado4);

//Exercicio 04

function adicionar(x){
    return function(y){
        return x + y;
    }
}

//arrow function let adicionar = op1 => op2 => op1 + op2 

resultado5 = adicionar(6)(8);
console.log(resultado5);

const divisivelPor = divisor => numero => !(numero % divisor);
const isDivisivel = divisivelPor(2);

console.log(isDivisivel(18));

//Exercicio 05

function arredondar(numero, precisao = 2){
    const fator = Math.pow(10, precisao)
    return Math.ceil(numero * fator) / fator
}

function imprimirBRL(valor){
    let qtdMilhares = 3;
    let separadorMilhar = '.'
    let separadorDecimal = ','

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(valor)%1)
    let parteInteira = Math.trunc(valor)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if (c % qtdMilhares == 0){
            stringBuffer.push(`${separadorMilhar} ${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        }else if (parteInteiraString.length < qtdMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? 'R$' : '-R$'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;
}


console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));