//Exercício 01

function multiplicar(multiplicador, ...valores ) {
    console.log(valores);
    console.log(multiplicador);
    let resultado = valores.map(numero => multiplicador * numero);
    console.log(resultado);
    return resultado;
}

console.log(multiplicar(2, 1,2,3,4,5));
 

//Exercício 02



function validaForm(form){
    
    if(form.nome.value == "" || form.nome.value.length < 10){
        alert("Preencha seu nome");
        form.nome.focus();
        form.nome.style.borderColor = "red";
        return false;
    }
    if(form.telefone.value == ""){
        alert("Preencha seu telefone");
        form.telefone.focus();
        form.telefone.style.borderColor = "red";
        return false;
    }
    if(form.email.value == "" || form.email.value.indexOf("@") == -1 || form.email.valueOf.indexOf(".") == -1){
        alert("Digite um email válido");
        form.email.focus();
        form.email.style.borderColor = "red";
        return false;
    }
    if(form.assunto.value == ""){
        alert("Preencha o assunto");
        form.assunto.focus();
        form.assunto.style.borderColor = "red";
        return false;
    }
    if(form.mensagem.value == ""){
        alert("Digite sua mensagem");
        form.mensagem.focus();
        form.mensagem.style.borderColor = "red";
        return false;
    }
}
