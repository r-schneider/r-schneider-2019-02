import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

/* const elemento = 
(<div>
    <ul>
        <li>Porto Alegre</li>
        <li>Novo Hamburgo</li>
        <li>Canoas</li>
    </ul>
</div>)
 */

 ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
