import React from 'react'

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            <div className="Card">
                <h2 className="pull-left text-center">{ episodio.nome }</h2>
                <img className="pull-left" src={ episodio.thumbUrl} alt={ episodio.nome}/>
                <h4 className="pull-right"> Já assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es), Nota: { episodio.nota || 'Sem Nota'}</h4>
                <h4>{ episodio.temporadaEpisodio }</h4>
                <h5>{ episodio.duracaoEmMin }</h5>
                <button className="App-btn pull-left" onClick={ props.sortearNoComp }>Próximo</button>
                <button className="App-btn pull-left" onClick={ props.marcarNoComp }>Marcar Como Assistido</button>    
            </div>    
        </React.Fragment>    
    )
}

export default EpisodioPadrao