import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './model/ListaEpisodios';
import EpisodioPadrao from './component/EpisodioPadrao'


class App extends Component {
  constructor(props){
    super(props)
    this.listaEpisodios = new ListaEpisodios()
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios
    }
    //console.log(this.listaEpisodios);
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state 
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  darNota(event){
    const { episodio } = this.state
    episodio.avaliar(event.target.value)
    this.setState({
      episodio,
      exibirMensagem: true
    })
    setTimeout(() => { this.setState({ exibirMensagem: false })}, 5000)
  }

  gerarCampoNota(){
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <label>Dê uma nota de 1 a 5:</label><br/>
              <input type="text" placeholder="1 a 5" onBlur={ this.darNota.bind(this) }/>
            </div>
          )
        }
      </div>              
    )
  }

  render(){
    const { episodio, exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="App-header">
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido }/>
          { this.gerarCampoNota() }
          <h4>{ exibirMensagem ? 'Nota Registrada!' : ' ' } </h4>
        </div>  
      </div>
    );  
  }
}

export default App;
