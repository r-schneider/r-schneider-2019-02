package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "INVENTARIOS_SEQ", sequenceName = "INVENTARIOS_SEQ")
public class Inventario extends AbstractEntity{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( generator = "INVENTARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_INVENTARIO", nullable = false)
    private Integer id;
    private Integer tamanho;

    @OneToMany( mappedBy = "inventario", cascade = CascadeType.ALL )
    private List<InventarioItem> inventarioItem = new ArrayList<>();

    @OneToOne(mappedBy = "inventario")
    private Personagem personagem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public List<InventarioItem> getItensInventarios() {
        return inventarioItem;
    }

    public void setItensInventarios(List<InventarioItem> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public Personagem getPersonagem() { return personagem; }

    public void setPersonagem(Personagem personagem) { this.personagem = personagem; }
}
