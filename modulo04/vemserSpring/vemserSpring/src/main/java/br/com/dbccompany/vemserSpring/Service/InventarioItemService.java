package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Entity.InventarioItem;
import br.com.dbccompany.vemserSpring.Entity.Item;
import br.com.dbccompany.vemserSpring.Repository.InventarioItemRepository;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import br.com.dbccompany.vemserSpring.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventarioItemService {

    @Autowired
    private InventarioItemRepository inventarioItemRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    @Autowired
    private ItemRepository itemRepository;


    @Transactional( rollbackFor = Exception.class )
    public InventarioItem salvarInventarioItem( InventarioItem inventarioItem ){
        Item itemObject = inventarioItem.getItem();
        if( itemObject.getId() == null ) {
            itemObject = itemRepository.save( itemObject );
        }

        Inventario inventarioObject = inventarioItem.getInventario();
        if( inventarioObject.getId() == null ) {
            inventarioObject = inventarioRepository.save( inventarioObject );
        }

        inventarioItem.setInventario( inventarioObject );
        inventarioItem.setItem( itemObject );

        return inventarioItemRepository.save( inventarioItem );
    }

    @Transactional( rollbackFor = Exception.class )
    public InventarioItem editarInventarioItem(Integer id, InventarioItem inventarioItem ){
        inventarioItem.setId(id);
        return inventarioItemRepository.save( inventarioItem );
    }

    public List<InventarioItem> listarInventarioItems(){
        return ( List<InventarioItem> ) inventarioItemRepository.findAll();
    }
}
