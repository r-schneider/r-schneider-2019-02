package br.com.dbccompany.vemserSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VemserSpringApplication {

		public static void main(String[] args) {
			SpringApplication.run(VemserSpringApplication.class, args);
		}

}
