package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ITENS_SEQ", sequenceName = "ITENS_SEQ")
public class Item extends AbstractEntity{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( generator = "ITENS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ITEM", nullable = false)
    private Integer id;
    private String descricao;

    @OneToMany( mappedBy = "item", cascade = CascadeType.ALL )
    private List<InventarioItem> inventarioItem = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<InventarioItem> getItensInventarios() {
        return inventarioItem;
    }

    public void setItensInventarios(List<InventarioItem> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
