package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository elfoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Inventario salvarInventario( Inventario elfo ){
        return elfoRepository.save( elfo );
    }

    @Transactional( rollbackFor = Exception.class )
    public Inventario editarInventario(Integer id, Inventario elfo ){
        elfo.setId(id);
        return elfoRepository.save( elfo );
    }

    public List<Inventario> listarInventarios(){
        return ( List<Inventario> ) elfoRepository.findAll();
    }
}
