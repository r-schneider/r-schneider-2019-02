package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ITENS_INVENTARIO_SEQ", sequenceName = "ITENS_INVENTARIO_SEQ")
public class InventarioItem extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( generator = "ITENS_INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ITENS_INVENTARIO", nullable = false)
    private Integer id;
    private Integer quantidade;

    @ManyToOne
    @JoinColumn( name = "FK_ID_INVENTARIO" )
    private Inventario inventario;

    @ManyToOne
    @JoinColumn( name = "FK_ID_ITEM" )
    private Item item;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
