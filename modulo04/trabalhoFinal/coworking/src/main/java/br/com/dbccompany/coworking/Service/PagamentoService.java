package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Pagamento salvarPagamento( Pagamento pagamento ){
        return pagamentoRepository.save( pagamento );
    }

    @Transactional( rollbackFor = Exception.class )
    public Pagamento editarPagamento(Integer id, Pagamento pagamento ){
        pagamento.setId(id);
        return pagamentoRepository.save( pagamento );
    }

    public List<Pagamento> listarPagamentos(){
        return ( List<Pagamento> ) pagamentoRepository.findAll();
    }
}