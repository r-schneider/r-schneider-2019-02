package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoRepository extends CrudRepository<Espaco, Integer> {

}
