package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
public class Cliente extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( nullable = false )
    private Integer id;

    @NotEmpty
    private String nome;

    @NotEmpty
    @CPF( message = "CPF inválido.")
    @Column( unique = true )
    private String cpf;

    @NotEmpty
    @Column( name = "DATA_NASCIMENTO")
    private LocalDate dataNascimento;

    @NotEmpty
    @ManyToOne
    @JoinColumn( name = "FK_ID_CONTATO" )
    @JsonBackReference
    private Contato contato;

    @OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<ClientePacote> clientePacote = new ArrayList<>();

    @OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public List<ClientePacote> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<ClientePacote> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
