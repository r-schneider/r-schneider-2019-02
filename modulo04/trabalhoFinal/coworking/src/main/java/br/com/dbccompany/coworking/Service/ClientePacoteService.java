package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientePacote;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientePacoteService {

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote salvarClientePacote( ClientePacote clientePacote ){
        return clientePacoteRepository.save( clientePacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacote editarClientePacote(Integer id, ClientePacote clientePacote ){
        clientePacote.setId(id);
        return clientePacoteRepository.save( clientePacote );
    }

    public List<ClientePacote> listarClientePacotes(){
        return ( List<ClientePacote> ) clientePacoteRepository.findAll();
    }
}