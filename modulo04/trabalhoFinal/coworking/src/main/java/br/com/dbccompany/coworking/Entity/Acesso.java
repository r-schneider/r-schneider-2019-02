package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
public class Acesso extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "FK_ID_CLIENTE_SALDO_CLIENTE"),
            @JoinColumn(name = "FK_ID_ESPACO_SALDO_CLIENTE")
    })
    private SaldoCliente saldoCliente;

    @Column(name = "IS_ENTRADA")
    private Boolean isEntrada;

    private LocalDate data;

    @Column(name = "IS_EXCECAO")
    private Boolean isExcecao;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
