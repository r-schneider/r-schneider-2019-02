package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacoPacote;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/espaco_pacote" )
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<EspacoPacote> todosEspacoPacotes(){
        return service.listarEspacoPacotes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public EspacoPacote novoEspacoPacote( @Valid @RequestBody EspacoPacote espacoPacote ) {
        return service.salvarEspacoPacote( espacoPacote );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacoPacote editarEspacoPacote( @PathVariable Integer id, @Valid @RequestBody EspacoPacote espacoPacote ) {
        return service.editarEspacoPacote( id, espacoPacote );
    }
}
