package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/acesso" )
public class AcessoController {

    @Autowired
    AcessoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Acesso> todosAcessos(){
        return service.listarAcessos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Acesso novoAcesso( @Valid @RequestBody Acesso acesso ) {
        return service.salvarAcesso( acesso );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acesso editarAcesso( @PathVariable Integer id, @Valid @RequestBody Acesso acesso ) {
        return service.editarAcesso( id, acesso );
    }
}
