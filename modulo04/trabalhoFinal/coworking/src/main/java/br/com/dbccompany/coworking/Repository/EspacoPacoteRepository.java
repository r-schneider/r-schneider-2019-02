package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoPacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Integer> {

}