package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@IdClass(SaldoClienteId.class)
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteId id;

    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Date vencimento;

    @OneToMany( mappedBy = "saldoCliente", cascade = CascadeType.ALL )
    private List<Acesso> acessoClientes = new ArrayList<>();

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acesso> getAcessoClientes() {
        return acessoClientes;
    }

    public void setAcessoClientes(List<Acesso> acessoClientes) {
        this.acessoClientes = acessoClientes;
    }
}
