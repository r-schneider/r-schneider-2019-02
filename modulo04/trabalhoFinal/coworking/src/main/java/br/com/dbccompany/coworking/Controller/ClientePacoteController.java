package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientePacote;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/cliente_pacote" )
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ClientePacote> todosClientePacotes(){
        return service.listarClientePacotes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public ClientePacote novoClientePacote( @Valid @RequestBody ClientePacote clientePacote ) {
        return service.salvarClientePacote( clientePacote );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientePacote editarClientePacote( @PathVariable Integer id, @Valid @RequestBody ClientePacote clientePacote ) {
        return service.editarClientePacote( id, clientePacote );
    }
}
