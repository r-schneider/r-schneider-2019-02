package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/saldo_cliente" )
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<SaldoCliente> todosSaldoClientes(){
        return service.listarSaldoClientes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public SaldoCliente novoSaldoCliente( @Valid @RequestBody SaldoCliente saldoCliente ) {
        return service.salvarSaldoCliente( saldoCliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editarSaldoCliente( @PathVariable Integer idCliente, @PathVariable Integer idEspaco, @Valid @RequestBody SaldoCliente saldoCliente ) {
        return service.editarSaldoCliente( idCliente, idEspaco, saldoCliente );
    }
}
