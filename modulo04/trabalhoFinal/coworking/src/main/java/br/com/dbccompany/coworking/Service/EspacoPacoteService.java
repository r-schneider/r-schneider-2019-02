package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacoPacote;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacote salvarEspacoPacote( EspacoPacote espacoPacote ){
        return espacoPacoteRepository.save( espacoPacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacote editarEspacoPacote(Integer id, EspacoPacote espacoPacote ){
        espacoPacote.setId(id);
        return espacoPacoteRepository.save( espacoPacote );
    }

    public List<EspacoPacote> listarEspacoPacotes(){
        return ( List<EspacoPacote> ) espacoPacoteRepository.findAll();
    }
}