package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/contato" )
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Contato> todosContatos(){
        return service.listarContatos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Contato novoContato( @Valid @RequestBody Contato contato ) {
        return service.salvarContato( contato );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contato editarContato( @PathVariable Integer id, @Valid @RequestBody Contato contato ) {
        return service.editarContato( id, contato );
    }
}
