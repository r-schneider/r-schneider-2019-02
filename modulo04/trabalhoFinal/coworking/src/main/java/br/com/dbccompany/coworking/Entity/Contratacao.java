package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
public class Contratacao extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @NotEmpty
    @ManyToOne
    @JoinColumn( name = "FK_ID_ESPACO" )
    private Espaco espaco;

    @NotEmpty
    @ManyToOne
    @JoinColumn( name = "FK_ID_CLIENTE" )
    private Cliente cliente;

    @NotEmpty
    private TipoContratacao tipoContratacao;

    @NotEmpty
    private Integer quantidade;

    private Integer desconto;

    @NotEmpty
    private Integer prazo;

    @OneToMany( mappedBy = "contratacao", cascade = CascadeType.ALL )
    private List<Pagamento> pagamentos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getDesconto() {
        return desconto;
    }

    public void setDesconto(Integer desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
