package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository espacoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Espaco salvarEspaco( Espaco espaco ){
        espaco.setValor("R$ "+ espaco.getValor());
        return espacoRepository.save( espaco );
    }

    @Transactional( rollbackFor = Exception.class )
    public Espaco editarEspaco(Integer id, Espaco espaco ){
        espaco.setId(id);
        return espacoRepository.save( espaco );
    }

    public List<Espaco> listarEspacos(){
        return ( List<Espaco> ) espacoRepository.findAll();
    }
}