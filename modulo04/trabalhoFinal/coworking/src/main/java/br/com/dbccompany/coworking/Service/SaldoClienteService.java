package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente salvarSaldoCliente( SaldoCliente saldoCliente ){
        return saldoClienteRepository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente editarSaldoCliente(Integer idCliente, Integer idEspaco, SaldoCliente saldoCliente ){
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setCliente( clienteRepository.findById( idCliente ).get() );
        saldoClienteId.setEspaco( espacoRepository.findById( idEspaco ).get() );
        saldoCliente.setId(saldoClienteId);
        return saldoClienteRepository.save( saldoCliente );
    }

    public List<SaldoCliente> listarSaldoClientes(){
        return ( List<SaldoCliente> ) saldoClienteRepository.findAll();
    }
}