package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/pagamento" )
public class PagamentoController {

    @Autowired
    PagamentoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Pagamento> todosPagamentos(){
        return service.listarPagamentos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Pagamento novoPagamento( @Valid @RequestBody Pagamento pagamento ) {
        return service.salvarPagamento( pagamento );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamento editarPagamento( @PathVariable Integer id, @Valid @RequestBody Pagamento pagamento ) {
        return service.editarPagamento( id, pagamento );
    }
}
