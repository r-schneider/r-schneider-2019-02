package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
public class EspacoPacote extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "FK_ID_ESPACO" )
    private Espaco espaco;

    @ManyToOne
    @JoinColumn( name = "FK_ID_PACOTE" )
    private Pacote pacote;

    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
