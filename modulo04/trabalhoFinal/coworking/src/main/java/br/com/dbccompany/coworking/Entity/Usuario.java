package br.com.dbccompany.coworking.Entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
public class Usuario extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;
    @NotEmpty
    @Column(nullable = false)
    private String nome;
    @NotEmpty
    @Email(message = "E-mail inválido.")
    @Column(nullable = false, unique = true)
    private String email;
    @NotEmpty
    @Column(nullable = false, unique = true)
    private String login;
    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9]+$", message= "Senha deve possuir apenas letras ou números")
    @Length(min = 6, message = "Senha deve ter pelo menos 6 caracteres.")
    @Column(nullable = false)
    private String senha;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
