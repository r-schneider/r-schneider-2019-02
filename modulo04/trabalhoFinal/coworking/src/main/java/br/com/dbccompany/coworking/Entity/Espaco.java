package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
public class Espaco extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;
    @NotEmpty
    @Column(nullable = false, unique = true)
    private String nome;
    @NotEmpty
    @Column(nullable = false)
    private Integer qtdPessoas;
    @NotEmpty
    @Column(nullable = false)
    private String valor;

    @OneToMany( mappedBy = "espaco", cascade = CascadeType.ALL )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "espaco", cascade = CascadeType.ALL )
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}