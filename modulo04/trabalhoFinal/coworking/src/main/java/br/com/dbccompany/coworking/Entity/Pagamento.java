package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
public class Pagamento extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "FK_ID_CLIENTE_PACOTE" )
    private ClientePacote clientePacote;

    @ManyToOne
    @JoinColumn( name = "FK_ID_CONTRATACAO" )
    private Contratacao contratacao;

    private TipoPagamento tipoPagamento;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
