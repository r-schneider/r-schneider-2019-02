package br.com.dbccompany.coworking.Security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Criptografia {

    private static MessageDigest msgDigest = null;

    static {
        try {
            msgDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private static char[] hexCodes( byte[] senha ) {
        char[] hexOutput = new char[senha.length * 2];
        String hexString;

        for( int i = 0; i < senha.length; i++ ) {
            hexString = "00" + Integer.toHexString( senha[i] );
            hexString.toUpperCase().getChars(hexString.length() - 2,
                    hexString.length(), hexOutput, i * 2);
        }
        return hexOutput;
    }

    public static String criptografar(String pwd) {
        if (msgDigest != null) {
            return new String(hexCodes(msgDigest.digest(pwd.getBytes())));
        }
        return null;
    }
}