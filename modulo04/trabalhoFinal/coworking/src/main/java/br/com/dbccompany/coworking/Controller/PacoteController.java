package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/pacote" )
public class PacoteController {

    @Autowired
    PacoteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Pacote> todosPacotes(){
        return service.listarPacotes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Pacote novoPacote( @Valid @RequestBody Pacote pacote ) {
        return service.salvarPacote( pacote );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pacote editarPacote( @PathVariable Integer id, @Valid @RequestBody Pacote pacote ) {
        return service.editarPacote( id, pacote );
    }
}