package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository pacoteRepository;

    @Transactional( rollbackFor = Exception.class )
    public Pacote salvarPacote( Pacote pacote ){
        pacote.setValor("R$ "+ pacote.getValor() );
        return pacoteRepository.save( pacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public Pacote editarPacote(Integer id, Pacote pacote ){
        pacote.setId(id);
        return pacoteRepository.save( pacote );
    }

    public List<Pacote> listarPacotes(){
        return ( List<Pacote> ) pacoteRepository.findAll();
    }
}