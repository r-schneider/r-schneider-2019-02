package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/contratacao" )
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Contratacao> todasContratacoes(){
        return service.listarContratacaos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Contratacao novaContratacao( @Valid @RequestBody Contratacao contratacao ) {
        return service.salvarContratacao( contratacao );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editarContratacao( @PathVariable Integer id, @Valid @RequestBody Contratacao contratacao ) {
        return service.editarContratacao( id, contratacao );
    }
}