package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/usuario" )
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Usuario> todosUsuarios(){
        return service.listarUsuarios();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Usuario novoUsuario( @Valid @RequestBody Usuario usuario ) {
        return service.salvarUsuario( usuario );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuario editarUsuario( @PathVariable Integer id, @Valid @RequestBody Usuario usuario ) {
        return service.editarUsuario( id, usuario );
    }
}
