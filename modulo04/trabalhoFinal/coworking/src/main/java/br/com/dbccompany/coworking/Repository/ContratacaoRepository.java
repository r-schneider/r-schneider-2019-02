package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {

}
