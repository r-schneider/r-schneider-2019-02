package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

}