package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Autowired
    private ContatoService contatoService;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvarCliente( Cliente cliente )  throws Exception{
        if(cliente.getContato() == null) {
            throw new Exception( "Cadastre um contato para este cliente." );
        }
        if( !cliente.getContato().getTipoContato().getNome().equalsIgnoreCase("EMAIL") && !cliente.getContato().getTipoContato().getNome().equalsIgnoreCase("TELEFONE" )) {
            throw new Exception( "Cliente precisa ter e-mail ou telefone cadastrado." );
        }

        tipoContatoService.salvarTipoContato(cliente.getContato().getTipoContato());
        contatoService.salvarContato(cliente.getContato());
        return clienteRepository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editarCliente(Integer id, Cliente cliente ){
        cliente.setId(id);
        return clienteRepository.save( cliente );
    }

    public List<Cliente> listarClientes(){
        return ( List<Cliente> ) clienteRepository.findAll();
    }
}