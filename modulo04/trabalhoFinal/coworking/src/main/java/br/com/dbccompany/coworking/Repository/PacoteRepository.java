package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<Pacote, Integer> {

}