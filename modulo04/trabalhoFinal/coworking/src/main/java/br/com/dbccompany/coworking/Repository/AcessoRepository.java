package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Acesso;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

}
