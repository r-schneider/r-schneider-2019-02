package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/tipo_contato" )
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<TipoContato> todosTipoContatos(){
        return service.listarTipoContatos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public TipoContato novoTipoContato( @Valid @RequestBody TipoContato tipoContato ) {
        return service.salvarTipoContato( tipoContato );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editarTipoContato( @PathVariable Integer id, @Valid @RequestBody TipoContato tipoContato ) {
        return service.editarTipoContato( id, tipoContato );
    }
}
