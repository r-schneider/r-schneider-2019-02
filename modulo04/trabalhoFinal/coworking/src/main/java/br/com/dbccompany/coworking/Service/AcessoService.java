package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Acesso salvarAcesso( Acesso acesso ){
        return acessoRepository.save( acesso );
    }

    @Transactional( rollbackFor = Exception.class )
    public Acesso editarAcesso(Integer id, Acesso acesso ){
        acesso.setId(id);
        return acessoRepository.save( acesso );
    }

    public List<Acesso> listarAcessos(){
        return ( List<Acesso> ) acessoRepository.findAll();
    }
}