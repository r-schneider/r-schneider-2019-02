package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Security.Criptografia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional( rollbackFor = Exception.class )
    public Usuario salvarUsuario( Usuario usuario ){
        usuario.setSenha(Criptografia.criptografar(usuario.getSenha()));
        return usuarioRepository.save( usuario );
    }

    @Transactional( rollbackFor = Exception.class )
    public Usuario editarUsuario(Integer id, Usuario usuario ){
        usuario.setId(id);
        return usuarioRepository.save( usuario );
    }

    public List<Usuario> listarUsuarios(){
        return ( List<Usuario> ) usuarioRepository.findAll();
    }
}