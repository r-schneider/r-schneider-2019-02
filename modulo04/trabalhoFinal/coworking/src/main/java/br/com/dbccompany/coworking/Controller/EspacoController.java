package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/espaco" )
public class EspacoController {

    @Autowired
    EspacoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Espaco> todosEspacos(){
        return service.listarEspacos();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Espaco novoEspaco( @Valid @RequestBody Espaco espaco ) {
            return service.salvarEspaco( espaco );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espaco editarEspaco( @PathVariable Integer id, @Valid @RequestBody Espaco espaco ) {
        return service.editarEspaco( id, espaco );
    }
}
