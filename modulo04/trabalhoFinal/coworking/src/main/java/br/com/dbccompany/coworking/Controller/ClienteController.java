package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping( "/coworking/cliente" )
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Cliente> todosClientes(){
        return service.listarClientes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Cliente novoCliente( @Valid @RequestBody Cliente cliente ) throws Exception {
        return service.salvarCliente( cliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cliente editarCliente( @PathVariable Integer id, @Valid @RequestBody Cliente cliente ) {
        return service.editarCliente( id, cliente );
    }
}