package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvarContato( Contato contato ){
        return contatoRepository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class )
    public Contato editarContato(Integer id, Contato contato ){
        contato.setId(id);
        return contatoRepository.save( contato );
    }

    public List<Contato> listarContatos(){
        return ( List<Contato> ) contatoRepository.findAll();
    }
}