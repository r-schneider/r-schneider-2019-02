package br.com.dbccompany.apiRPG.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //config http
        http.headers().frameOptions().sameOrigin().and().csrf().disable().authorizeRequests()
                .antMatchers("/home").permitAll().and().authorizeRequests()
                .antMatchers( HttpMethod.POST, "/login" ).permitAll().anyRequest().authenticated().and().cors().and()
                .addFilterBefore( new JWTLoginFilter( "/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class )
                .addFilterBefore( new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class );
    }

    @Override
    public void configure( AuthenticationManagerBuilder auth ) throws Exception {
        //config login
        auth.inMemoryAuthentication().withUser( "admin" ).password( "{noop}password" )
                .roles( "ADMIN" );
    }
}
