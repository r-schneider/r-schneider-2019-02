package br.com.dbccompany.apiRPG.Entity;

import javax.persistence.Entity;

@Entity
public class Anao extends Personagem {

    public Anao() {
        super.setRaca(TipoRaca.ANAO);
    }
}