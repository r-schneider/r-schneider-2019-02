package br.com.dbccompany.apiRPG.Repository;

import br.com.dbccompany.apiRPG.Entity.Anao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnaoRepository extends CrudRepository<Anao, Integer> {

}