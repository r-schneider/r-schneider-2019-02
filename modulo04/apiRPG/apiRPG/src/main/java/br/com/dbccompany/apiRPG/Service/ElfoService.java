package br.com.dbccompany.apiRPG.Service;

import br.com.dbccompany.apiRPG.Entity.Elfo;
import br.com.dbccompany.apiRPG.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ElfoService {

    @Autowired
    private ElfoRepository elfoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Elfo salvarElfo( Elfo elfo ){
        return elfoRepository.save( elfo );
    }

    @Transactional( rollbackFor = Exception.class )
    public Elfo editarElfo(Integer id, Elfo elfo ){
        elfo.setId(id);
        return elfoRepository.save( elfo );
    }

    public List<Elfo> listarElfos(){
        return ( List<Elfo> ) elfoRepository.findAll();
    }
}

