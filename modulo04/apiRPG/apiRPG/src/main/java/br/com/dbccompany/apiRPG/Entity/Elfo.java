package br.com.dbccompany.apiRPG.Entity;

import javax.persistence.Entity;

@Entity
public class Elfo extends Personagem {

    public Elfo() {
        super.setRaca(TipoRaca.ELFO);
    }
}