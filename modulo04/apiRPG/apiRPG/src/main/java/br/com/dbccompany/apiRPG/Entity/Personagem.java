package br.com.dbccompany.apiRPG.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "PERSONAGENS_SEQ", sequenceName = "PERSONAGENS_SEQ")
public abstract class Personagem extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( generator = "PERSONAGENS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_PERSONAGEM", nullable = false )
    private Integer id;
    private String nome;
    private Double vida;
    private Integer experiencia;

    @Enumerated(EnumType.STRING)
    private TipoRaca raca;

    @Enumerated(EnumType.STRING)
    private TipoStatus status;

    //@OneToOne(cascade = CascadeType.ALL)
    //@JoinColumn(name = "ID_INVENTARIO")
    // private Inventario inventario;

    //private Integer qtdExpPorAtaque;
    //private Double qtdDanoPorAtaque;
    //private Double qtdDanoRecebido;

    public Integer getId() {
        return id;
    }

    @Override
    public void setId( Integer id ) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public TipoRaca getRaca() {
        return raca;
    }

    public void setRaca(TipoRaca raca) {
        this.raca = raca;
    }

    public TipoStatus getStatus() {
        return status;
    }

    public void setStatus(TipoStatus status) {
        this.status = status;
    }

}
