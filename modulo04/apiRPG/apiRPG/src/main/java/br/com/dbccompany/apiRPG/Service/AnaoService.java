package br.com.dbccompany.apiRPG.Service;

import br.com.dbccompany.apiRPG.Entity.Anao;
import br.com.dbccompany.apiRPG.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnaoService {

    @Autowired
    private AnaoRepository anaoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Anao salvarAnao( Anao anao ){
        return anaoRepository.save( anao );
    }

    @Transactional( rollbackFor = Exception.class )
    public Anao editarAnao(Integer id, Anao anao ){
        anao.setId(id);
        return anaoRepository.save( anao );
    }

    public List<Anao> listarAnoes(){
        return ( List<Anao> ) anaoRepository.findAll();
    }
}

