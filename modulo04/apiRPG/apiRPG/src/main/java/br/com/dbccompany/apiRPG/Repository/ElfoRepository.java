package br.com.dbccompany.apiRPG.Repository;

import br.com.dbccompany.apiRPG.Entity.Elfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElfoRepository extends CrudRepository<Elfo, Integer> {

}