import React, { Component } from 'react';
import RpgApi from '../RpgApi';

import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';

import Login from './Login';

export default class Home extends Component {
    
    constructor( props ) {
        super( props )
        this.rpg = new RpgApi()
        this.login = new Login()
        this.state = {
            validador: '',
            elfos: []
        }
    }

    async getElfos () {
        const elfos = await this.rpg.listarElfos()
        this.setState({
            validador: true,
            elfos
        })
    }
    
    componentDidMount() {
        this.getElfos();
    }

    renderElfos() {
        const elfosMap  = this.state.elfos.map(( e ) => ( <li key={ e.id }>Id: {e.id} - Nome: { e.nome } <hr/><br/></li> ))
        return(
            <div>
            {
                this.state.validador && (
                    <div>
                        <List> { elfosMap } </List>
                    </div>
                )
            }
            </div>    
        )
    }

    render() {
        return (
            <div>
                <Button variant="contained" color="secondary" onClick={ this.login.logout.bind(this) } href="/login">Logout</Button><br/><br/>
                <Button variant="contained" color="primary" href="/inserir">Inserir Elfo</Button>
                <h1>LOGADO</h1>
                { console.log(this.state.elfos)} 
                <h2>{ this.renderElfos() }</h2>
            </div>
        )    
    }        
}