import React, { Component } from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Login from './Login';


export default class Inserir extends Component {
    constructor(props) {
        super(props)
        this.login = new Login()
        this.state = {
            nome: ''
        }
    }
    
    trocaValoresState( event ) {
        const { name, value } = event.target;
        this.setState({ [name]: value })
    }

    inserir( event ) {
        event.preventDefault();
        const AuthStr = localStorage.getItem('Authorization')
        const { nome } = this.state
        if( nome !== '' ) {
            axios.post('http://localhost:8080/rpg/char/novo', {
                nome: this.state.nome
            }, { headers: { Authorization: AuthStr } }).then( this.props.history.push('/home') )   
        }
    }

    render() {
        return (
            <div>
                <Button variant="contained" color="secondary" onClick={ this.login.logout.bind(this) } href="/login">Logout</Button><br/><br/>
                <form>
                <TextField required label="Nome" id="standard-required" name="nome" placeholder="Digite o nome do personagem" onChange={ this.trocaValoresState.bind( this) }/>
                <br/>
                <br/>
                <Button variant="contained" color="primary" onClick={ this.inserir.bind(this) }>Enviar</Button>
                </form>
            </div>
        )
    }
}