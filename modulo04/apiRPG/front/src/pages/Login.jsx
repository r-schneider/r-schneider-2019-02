import React, { Component } from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    
    trocaValoresState( event ) {
        const { name, value } = event.target;
        this.setState({ [name]: value })
    }

    logar( event ) {
        event.preventDefault();

        const { username, password } = this.state
        if( username && password ) {
            axios.post('http://localhost:8080/login', {
                username: this.state.username,
                password: this.state.password
            }).then( response => {
                localStorage.setItem('Authorization', response.headers.authorization);
                this.props.history.push('/home') 
                }
            )
        }
    }

    logout() {
        localStorage.removeItem( 'Authorization' );
    }

    render() {
        return (
            <div>
                <TextField required label="Username" id="standard-required" name="username" placeholder="Digite seu usuário" onChange={ this.trocaValoresState.bind( this) }/><br/>
                <TextField required label="Password" id="standard-required" name="password" placeholder="Digite sua senha" onChange={ this.trocaValoresState.bind( this) }/><br/><br/>
                <Button variant="contained" color="primary" onClick={ this.logar.bind(this) }>LogIn</Button>
            </div>
        )
    }
}