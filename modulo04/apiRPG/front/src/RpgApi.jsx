import { Component } from 'react';
import axios from 'axios';

export default class RpgApi extends Component {

    async listarElfos () {
        console.log('Teste')
        const AuthStr = localStorage.getItem('Authorization')
        const URL = 'http://localhost:8080/rpg/char/'
        const { data } = await axios.get(URL, { headers: { Authorization: AuthStr } })
        const retorno = data
        return retorno
    }
}