import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';

import Inserir from './pages/Inserir';
import Home from './pages/Home';
import Login from './pages/Login';

import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
            <React.Fragment>
                <Route path="/" exact component={ Login } />
                <Route path="/login" exact component={ Login } />
                <PrivateRoute path="/home" exact component={ Home } />
                <PrivateRoute path="/inserir" exact component={ Inserir } />
            </React.Fragment>
        </Router>  
      </div>
    );
  }
}

export default App;
