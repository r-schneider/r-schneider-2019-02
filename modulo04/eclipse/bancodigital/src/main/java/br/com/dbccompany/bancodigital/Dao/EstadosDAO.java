package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO<Estados> {
	
	PaisesDAO dao = new PaisesDAO();
	
	public Estados parseFrom( EstadosDTO dto ) {
		Estados estado = null;
		if( dto.getId() != null ) {
			estado = search( dto.getId());
		} else {
			estado = new Estados();
		}
		estado.setNome( dto.getEstado() );
		estado.setPais( dao.parseFrom(dto.getPais()) );
		
		return estado;
	}
	
	@Override
	protected Class<Estados> getEntityClass() {
		return Estados.class;
	}
}