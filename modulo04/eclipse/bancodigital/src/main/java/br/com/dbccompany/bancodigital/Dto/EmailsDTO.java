package br.com.dbccompany.bancodigital.Dto;

public class EmailsDTO {
	public Integer id;
	public String email;
	public ClientesDTO cliente;
	
	public Integer getId() {
		return id;
	}
	
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	public ClientesDTO getCliente() {
		return cliente;
	}
	
	public void setCliente( ClientesDTO cliente ) {
		this.cliente = cliente;
	}
}
