package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class BairrosService {
	
	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	private static final Logger LOG = Logger.getLogger( BairrosService.class.getName());

	public void salvarBairros( BairrosDTO bairroDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bairros bairro = BAIRROS_DAO.parseFrom( bairroDTO );
		
		try {
			Bairros bairroRes = BAIRROS_DAO.search( 1 );
			if( bairroRes == null ) {	
				BAIRROS_DAO.create( bairro );
			} else {
				bairro.setId( bairroRes.getId());
				BAIRROS_DAO.update( bairro );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		bairroDTO.setId( bairro.getId() );
	}
}
