package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_CORRENTISTA", nullable = false)
	private Integer id;
	
	@Column( name = "RAZAO_SOCIAL")
	private String razaoSocial;
	
	private String cnpj;
	
	private TipoCorrentistas tipo;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "CORRENTISTAS_X_CLIENTES",
		joinColumns = { @JoinColumn( name = "FK_ID_CORRENTISTA" )},
		inverseJoinColumns = { @JoinColumn( name = "FK_ID_CLIENTE" )})
	
	private List<Clientes> clientes = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "AGENCIAS_X_CORRENTISTAS",
		joinColumns = { @JoinColumn( name = "FK_ID_CORRENTISTA" )},
		inverseJoinColumns = { @JoinColumn( name = "FK_ID_AGENCIA" )})
	
	private List<Agencias> agencias = new ArrayList<>();

	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public TipoCorrentistas getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoCorrentistas tipo) {
		this.tipo = tipo;
	}
	
	public List<Agencias> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
}
	