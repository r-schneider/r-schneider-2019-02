package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "TELEFONES" )
@SequenceGenerator( allocationSize = 1, name = "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ")
public class Telefones extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "TELEFONES_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_TELEFONE", nullable = false)
	private Integer id;
	
	private String numero;
	
	@Enumerated( EnumType.STRING )
	private TipoTelefones tipo;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "CLIENTES_X_TELEFONES",
		joinColumns = { @JoinColumn( name = "FK_ID_TELEFONE" )},
		inverseJoinColumns = { @JoinColumn( name = "FK_ID_CLIENTE" )})
	
	private List<Clientes> clientes = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public TipoTelefones getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoTelefones tipo) {
		this.tipo = tipo;
	}
	
	public List<Clientes> getClientes() {
		return clientes;
	}
	
	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
	
}	
	