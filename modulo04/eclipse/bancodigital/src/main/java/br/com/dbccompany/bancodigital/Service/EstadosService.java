package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EstadosDAO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EstadosService {
	
	private static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
	private static final Logger LOG = Logger.getLogger( EstadosService.class.getName());

	public void salvarEstados( EstadosDTO estadoDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Estados estado = ESTADOS_DAO.parseFrom( estadoDTO );
		
		try {
			Estados estadoRes = ESTADOS_DAO.search( 1 );
			if( estadoRes == null ) {	
				ESTADOS_DAO.create( estado );
			} else {
				estado.setId( estadoRes.getId());
				ESTADOS_DAO.update( estado );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		estadoDTO.setId( estado.getId() );
	}
}
