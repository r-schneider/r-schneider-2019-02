package br.com.dbccompany.bancodigital.Dto;

import java.util.List;

public class AgenciasDTO {
	
	private Integer id;
	private Integer codigo;
	private String agencia;
	
	private EnderecosDTO endereco;
	private BancosDTO banco;
	
	private List<CorrentistasDTO> correntistas;

	public Integer getId() {
		return id;
	}
	
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo( Integer codigo ) {
		this.codigo = codigo;
	}
	
	public String getAgencia() {
		return agencia;
	}
	
	public void setAgencia( String agencia ) {
		this.agencia = agencia;
	}
	
	public EnderecosDTO getEndereco() {
		return endereco;
	}
	
	public void setEndereco( EnderecosDTO endereco ) {
		this.endereco = endereco;
	}
	
	public BancosDTO getBanco() {
		return banco;
	}
	
	public void setBanco( BancosDTO banco ) {
		this.banco = banco;
	}
	
	public List<CorrentistasDTO> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<CorrentistasDTO> correntistas) {
		this.correntistas = correntistas;
	}
}
