package br.com.dbccompany.bancodigital.Dto;

public class BairrosDTO {

	private Integer id;
	private String bairro;
	
	private CidadesDTO cidade;

	public Integer getId() {
		return id;
	}

	public void setId (Integer id ) {
		this.id = id;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro( String bairro ) {
		this.bairro = bairro;
	}

	public CidadesDTO getCidade() {
		return cidade;
	}

	public void setCidade( CidadesDTO cidade ) {
		this.cidade = cidade;
	}
}
