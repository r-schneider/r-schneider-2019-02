package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos> {
	
	BairrosDAO dao = new BairrosDAO();
	
	public Enderecos parseFrom( EnderecosDTO dto ) {
		Enderecos endereco = null;
		if( dto.getId() != null ) {
			endereco = search( dto.getId());
		} else {
			endereco = new Enderecos();
		}
		endereco.setLogradouro( dto.getLogradouro() );
		endereco.setNumero( dto.getNumero() );
		endereco.setComplemento( dto.getComplemento() );
		
		
		endereco.setBairro( dao.parseFrom(dto.getBairro()) );
		
		return endereco;
	}
	
	@Override
	protected Class<Enderecos> getEntityClass() {
		return Enderecos.class;
	}
}
