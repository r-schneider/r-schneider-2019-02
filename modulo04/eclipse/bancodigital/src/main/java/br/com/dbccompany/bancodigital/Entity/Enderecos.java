package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ENDERECOS_SEQ", sequenceName = "ENDERECOS_SEQ")
public class Enderecos extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "ENDERECOS_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_ENDERECO", nullable = false)
	private Integer id;
	
	private String logradouro;
	
	private Integer numero;
	
	private String complemento;
	
	@ManyToOne
	@JoinColumn( name = "FK_ID_BAIRRO" )
	private Bairros bairro;
	
	
    @OneToOne(mappedBy = "endereco")
    private Agencias agencia;
    
	@OneToMany( mappedBy = "endereco", cascade = CascadeType.ALL )
	private List<Clientes> clientes = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public Bairros getBairro() {
		return bairro;
	}
	
	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}
	
	public Agencias getAgencia() {
		return agencia;
	}
	
	public void setAgencia(Agencias agencia) {
		this.agencia = agencia;
	}
	
	public List<Clientes> getClientes() {
		return clientes;
	}
	
	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
}