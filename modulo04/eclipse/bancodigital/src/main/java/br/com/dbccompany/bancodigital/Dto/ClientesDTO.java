package br.com.dbccompany.bancodigital.Dto;

public class ClientesDTO {
	
	private Integer id;
	private String cpf;
	private String nome;
	private EnderecosDTO endereco;
	
	public Integer getId() {
		return id;
	}
	
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf( String cpf ) {
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome( String nome ) {
		this.nome = nome;
	}
	
	public EnderecosDTO getEndereco() {
		return endereco;
	}
	
	public void setEndereco( EnderecosDTO endereco ) {
		this.endereco = endereco;
	}
}
