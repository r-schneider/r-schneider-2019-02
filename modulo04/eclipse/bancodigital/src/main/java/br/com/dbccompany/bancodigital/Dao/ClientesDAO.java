package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesDAO extends AbstractDAO<Clientes> {
	
	EnderecosDAO dao = new EnderecosDAO();
	
	public Clientes parseFrom( ClientesDTO dto ) {
		Clientes cliente = null;
		if( dto.getId() != null ) {
			cliente = search( dto.getId());
		} else {
			cliente = new Clientes();
		}
		cliente.setNome( dto.getNome() );
		cliente.setCpf( dto.getCpf() );
		
		cliente.setEndereco( dao.parseFrom(dto.getEndereco()) );
		
		return cliente;
	}
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}
}
