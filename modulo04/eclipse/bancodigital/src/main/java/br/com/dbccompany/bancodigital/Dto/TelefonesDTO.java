package br.com.dbccompany.bancodigital.Dto;

public class TelefonesDTO {
	
	public Integer id;
	public String numero;
	
	public Integer getId() {
		return id;
	}
	public void setId( Integer id ) {
		this.id = id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero( String numero ) {
		this.numero = numero;
	}
}
