package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ")
public class Estados extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_ESTADO", nullable = false)
	private Integer id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn( name = "FK_ID_PAIS" )
	private Paises pais;
	
	@OneToMany( mappedBy = "estado", cascade = CascadeType.ALL )
	private List<Cidades> cidades = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Paises getPais() {
		return pais;
	}
	
	public void setPais(Paises pais) {
		this.pais = pais;
	}
	
	public List<Cidades> getCidades() {
		return cidades;
	}
	
	public void setCidades(List<Cidades> cidades) {
		this.cidades = cidades;
	}
}