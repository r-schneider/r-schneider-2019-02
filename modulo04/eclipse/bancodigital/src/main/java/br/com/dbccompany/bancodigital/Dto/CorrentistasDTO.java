package br.com.dbccompany.bancodigital.Dto;

import java.util.List;

import br.com.dbccompany.bancodigital.Entity.TipoCorrentistas;

public class CorrentistasDTO {
	
	public Integer id;
	public String razaoSocial;
	public String cnpj;
	private TipoCorrentistas tipo;
	
	List<AgenciasDTO> agencias;
	
	public Integer getId() {
		return id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial( String razaoSocial ) {
		this.razaoSocial = razaoSocial;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj( String cnpj ) {
		this.cnpj = cnpj;
	}
	
	public TipoCorrentistas getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoCorrentistas tipo) {
		this.tipo = tipo;
	}
	
	public List<AgenciasDTO> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<AgenciasDTO> agencias) {
		this.agencias = agencias;
	}
}
