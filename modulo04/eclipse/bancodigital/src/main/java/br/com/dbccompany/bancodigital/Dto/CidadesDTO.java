package br.com.dbccompany.bancodigital.Dto;

public class CidadesDTO {

	private Integer id;
	private String cidade;
	
	private EstadosDTO estado;

	public Integer getId() {
		return id;
	}

	public void setId ( Integer id ) {
		this.id = id;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade( String cidade ) {
		this.cidade = cidade;
	}

	public EstadosDTO getEstado() {
		return estado;
	}

	public void setEstado( EstadosDTO estado ) {
		this.estado = estado;
	}
}
