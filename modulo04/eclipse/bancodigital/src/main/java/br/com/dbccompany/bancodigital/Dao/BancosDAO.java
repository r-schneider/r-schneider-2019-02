package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class BancosDAO extends AbstractDAO<Bancos> {
	
	public Bancos parseFrom( BancosDTO dto ) {
		Bancos banco = null;
		if( dto.getId() != null ) {
			banco = search( dto.getId());
		} else {
			banco = new Bancos();
		}
		
		banco.setCodigo( dto.getCodigo() );
		banco.setNome( dto.getBanco() );
		
		return banco;
	}
	
	@Override
	protected Class<Bancos> getEntityClass() {
		return Bancos.class;
	}
}
