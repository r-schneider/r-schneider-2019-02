package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BancosDAO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class BancosService {
	
	private static final BancosDAO BANCOS_DAO = new BancosDAO();
	private static final Logger LOG = Logger.getLogger( BancosService.class.getName());

	public void salvarBancos( BancosDTO bancoDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bancos banco = BANCOS_DAO.parseFrom( bancoDTO );
		
		try {
			Bancos bancoRes = BANCOS_DAO.search( 1 );
			if( bancoRes == null ) {	
				BANCOS_DAO.create( banco );
			} else {
				banco.setId( bancoRes.getId());
				BANCOS_DAO.update( banco );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		bancoDTO.setId( banco.getId() );
	}
}
