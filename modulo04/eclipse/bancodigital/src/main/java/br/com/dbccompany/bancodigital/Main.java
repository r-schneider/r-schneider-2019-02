package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.TipoCorrentistas;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {
	
	public static void main( String[] args ) {
		
		PaisesService paisService = new PaisesService();
		EstadosService estadoService = new EstadosService();
		CidadesService cidadeService = new CidadesService();
		BairrosService bairroService = new BairrosService();
		EnderecosService enderecoService = new EnderecosService();
		BancosService bancoService = new BancosService();
		AgenciasService agenciaService = new AgenciasService();
		ClientesService clienteService = new ClientesService();
		CorrentistasService correntistaService = new CorrentistasService();

		PaisesDTO pais = new PaisesDTO();
		EstadosDTO estado = new EstadosDTO();
		CidadesDTO cidade = new CidadesDTO();
		
		BairrosDTO bairroAlfa = new BairrosDTO();
		BairrosDTO bairroBeta = new BairrosDTO();
		
		EnderecosDTO endAg01 = new EnderecosDTO();
		EnderecosDTO endAg02 = new EnderecosDTO();
		EnderecosDTO endCl01 = new EnderecosDTO();
		EnderecosDTO endCl02 = new EnderecosDTO();
		
		BancosDTO bancoAlfa = new BancosDTO();
		BancosDTO bancoBeta = new BancosDTO();
		
		AgenciasDTO agAlfa = new AgenciasDTO();
		
		CorrentistasDTO correntistaAlfa = new CorrentistasDTO();
		
		List<CorrentistasDTO> listaCorrentistas = new ArrayList<CorrentistasDTO>();
		List<AgenciasDTO> listaAgencias = new ArrayList<AgenciasDTO>();
		
		pais.setPais( "Brasil" );
		paisService.salvarPaises( pais );
		
		estado.setEstado( "Rio Grande do Sul" );
		estado.setPais(pais);
		estadoService.salvarEstados( estado );	

		cidade.setCidade("Canoas");
		cidade.setEstado( estado );
		cidadeService.salvarCidades( cidade );
		
		bairroAlfa.setBairro( "Niteroi" );
		bairroAlfa.setCidade( cidade );
		bairroService.salvarBairros( bairroAlfa );
		
		bairroBeta.setBairro( "Centro" );
		bairroBeta.setCidade( cidade );
		bairroService.salvarBairros( bairroBeta );
		
		endAg01.setLogradouro( "Rua XV de Janeiro" );
		endAg01.setNumero(178);
		endAg01.setBairro(bairroBeta);
		enderecoService.salvarEnderecos( endAg01 );
		
		endAg02.setLogradouro( "Rua XV de Janeiro" );
		endAg02.setNumero(579);
		endAg02.setBairro(bairroBeta);
		enderecoService.salvarEnderecos( endAg02 );
		
		endCl01.setLogradouro( "Rua Dom Pedro II" );
		endCl01.setNumero(601);
		endCl01.setBairro(bairroAlfa);
		enderecoService.salvarEnderecos( endCl01 );
		
		endCl02.setLogradouro( "Rua Curupaiti" );
		endCl02.setNumero(178);
		endCl02.setBairro(bairroAlfa);
		enderecoService.salvarEnderecos( endCl02 );
		
		bancoAlfa.setBanco( "Itau" );
		bancoAlfa.setCodigo(1011);
		bancoService.salvarBancos( bancoAlfa );
		
		bancoBeta.setBanco( "Nubank" );
		bancoBeta.setCodigo(753);
		bancoService.salvarBancos( bancoBeta );
		
		listaAgencias.add( agAlfa );

		correntistaAlfa.setCnpj("53733184000165");
		correntistaAlfa.setRazaoSocial("Restaurante Maia");
		correntistaAlfa.setTipo(TipoCorrentistas.PJ);
		correntistaService.salvarCorrentistas( correntistaAlfa );
		
		listaCorrentistas.add( correntistaAlfa );
		
		agAlfa.setAgencia( "Itau Canoas" );
		agAlfa.setBanco( bancoAlfa );
		agAlfa.setCodigo( 533 );
		agAlfa.setEndereco( endAg01 );
		agAlfa.setCorrentistas( listaCorrentistas );	
		agenciaService.salvarAgencias( agAlfa );
		
//		AgenciasDTO agBeta = new AgenciasDTO();
//		agBeta.setAgencia("Nubank RS");
//		agBeta.setBanco( bancoBeta );
//		agBeta.setCodigo(533);
//		agBeta.setEndereco( endAg02 );
//		agenciaService.salvarAgencias( agBeta );
		
//		CorrentistasDTO correntistaBeta = new CorrentistasDTO();
//		correntistaBeta.setCnpj("45716088000180");
//		correntistaBeta.setRazaoSocial("Nightcrawler Pub");
//		correntistaBeta.setTipo(TipoCorrentistas.PJ);
//		correntistaService.salvarCorrentistas( correntistaBeta );
//		
//		ClientesDTO clienteAlfa = new ClientesDTO();
//		clienteAlfa.setCpf("04550096004");
//		clienteAlfa.setNome("Renato Mendes");
//		clienteAlfa.setEndereco(endCl01);
//		clienteService.salvarClientes(clienteAlfa);
//		
//		ClientesDTO clienteBeta = new ClientesDTO();
//		clienteBeta.setCpf("90876047002");
//		clienteBeta.setNome("Fernando Maia");
//		clienteBeta.setEndereco(endCl02);
//		clienteService.salvarClientes(clienteBeta);
		
		System.exit( 0 );
	}
}
