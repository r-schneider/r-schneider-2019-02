package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesDAO extends AbstractDAO<Cidades> {
	
	EstadosDAO dao = new EstadosDAO();
	
	public Cidades parseFrom( CidadesDTO dto ) {
		Cidades cidade = null;
		if( dto.getId() != null ) {
			cidade = search( dto.getId());
		} else {
			cidade = new Cidades();
		}
		cidade.setNome( dto.getCidade() );
		
		cidade.setEstado( dao.parseFrom(dto.getEstado()) );
		
		return cidade;
	}
	
	@Override
	protected Class<Cidades> getEntityClass() {
		return Cidades.class;
	}
}