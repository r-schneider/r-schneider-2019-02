package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class CidadesService {
	
	private static final CidadesDAO CIDADES_DAO = new CidadesDAO();
	private static final Logger LOG = Logger.getLogger( CidadesService.class.getName());

	public void salvarCidades( CidadesDTO cidadeDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cidades cidade = CIDADES_DAO.parseFrom( cidadeDTO );
		
		try {
			Cidades cidadeRes = CIDADES_DAO.search( 1 );
			if( cidadeRes == null ) {	
				CIDADES_DAO.create( cidade );
			} else {
				cidade.setId( cidadeRes.getId());
				CIDADES_DAO.update( cidade );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		cidadeDTO.setId( cidade.getId() );
	}
}
