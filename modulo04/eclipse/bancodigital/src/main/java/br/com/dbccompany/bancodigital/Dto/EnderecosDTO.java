package br.com.dbccompany.bancodigital.Dto;

public class EnderecosDTO {

	private Integer id;
	private String logradouro;
	private int numero;
	private String complemento;
	
	private BairrosDTO bairro;

	public Integer getId() {
		return id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro( String logradouro ) {
		this.logradouro = logradouro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero( int numero ) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento( String complemento ) {
		this.complemento = complemento;
	}
	
	public BairrosDTO getBairro() {
		return bairro;
	}
	

	public void setBairro( BairrosDTO bairro ) {
		this.bairro = bairro;
	}
	
}
