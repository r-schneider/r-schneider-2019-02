package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class PaisesDAO extends AbstractDAO<Paises> {

	public Paises parseFrom( PaisesDTO dto ) {
		Paises pais = null;
		if( dto.getId() != null ) {
			pais = search( dto.getId());
		} else {
			pais = new Paises();
		}
		pais.setNome( dto.getPais() );
		
		return pais;
	}
	
	@Override
	protected Class<Paises> getEntityClass() {
		return Paises.class;
	}
	
}
