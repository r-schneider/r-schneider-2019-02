package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class AgenciasService {
	
	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	private static final Logger LOG = Logger.getLogger( AgenciasService.class.getName());

	public void salvarAgencias( AgenciasDTO agenciaDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Agencias agencia = AGENCIAS_DAO.parseFrom( agenciaDTO );
		
		try {
			Agencias agenciaRes = AGENCIAS_DAO.search( 1 );
			if( agenciaRes == null ) {	
				AGENCIAS_DAO.create( agencia );
			} else {
				agencia.setId( agenciaRes.getId());
				AGENCIAS_DAO.update( agencia );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		agenciaDTO.setId( agencia.getId() );
	}
}
