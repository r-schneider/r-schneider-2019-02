package br.com.dbccompany.bancodigital.Entity;

public enum TipoEstadoCivil {
	SOLTEIRO, CASADO, DIVORCIADO, VIUVO;
}
