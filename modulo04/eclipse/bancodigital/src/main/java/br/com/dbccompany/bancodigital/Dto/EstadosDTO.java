package br.com.dbccompany.bancodigital.Dto;

public class EstadosDTO {
	
	
	
	private Integer id;
	private String estado;
	
	private PaisesDTO pais;

	public Integer getId() {
		return id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado( String estado ) {
		this.estado = estado;
	}

	public PaisesDTO getPais() {
		return pais;
	}

	public void setPais( PaisesDTO pais ) {
		this.pais = pais;
	}
}
