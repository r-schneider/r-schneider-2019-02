package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EmailsDAO;
import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.Emails;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EmailsService {
	
	private static final EmailsDAO EMAILS_DAO = new EmailsDAO();
	private static final Logger LOG = Logger.getLogger( EmailsService.class.getName());
	
	public void salvarEmails( EmailsDTO emailDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Emails email = EMAILS_DAO.parseFrom( emailDTO );
		
		try {
			Emails emailRes = EMAILS_DAO.search( 1 );
			if( emailRes == null ) {	
				EMAILS_DAO.create( email );
			} else {
				email.setId( emailRes.getId());
				EMAILS_DAO.update( email );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		emailDTO.setId( email.getId() );
	}
}
