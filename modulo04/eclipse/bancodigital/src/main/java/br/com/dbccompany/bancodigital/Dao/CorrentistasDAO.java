package br.com.dbccompany.bancodigital.Dao;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas> {
	
	public Correntistas parseFrom( CorrentistasDTO dto ) {
		Correntistas correntista = null;
		
		AgenciasDAO agenciaDAO = new AgenciasDAO();
		
		if( dto.getId() != null ) {
			correntista = search( dto.getId());
		} else {
			correntista = new Correntistas();
		}
		correntista.setRazaoSocial( dto.getRazaoSocial() );
		correntista.setCnpj( dto.getCnpj() );
		
		List<AgenciasDTO> agenciasDTO = dto.getAgencias();
		List<Agencias> agencias = new ArrayList<>();
		for(int i = 0; i < agenciasDTO.size();i++)
		{
			AgenciasDTO agencia = agenciasDTO.get(i);
			agencias.add(agenciaDAO.parseFrom(agencia));
		}
		
		return correntista;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
}
