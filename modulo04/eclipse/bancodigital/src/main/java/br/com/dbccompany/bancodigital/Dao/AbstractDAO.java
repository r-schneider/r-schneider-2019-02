package br.com.dbccompany.bancodigital.Dao;

import java.util.List;

import org.hibernate.Session;

import br.com.dbccompany.bancodigital.Entity.AbstractEntity;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public abstract class AbstractDAO<Entity extends AbstractEntity> {
	
	protected abstract Class<Entity> getEntityClass();
	
	@SuppressWarnings("unchecked")
    public Entity search(Integer id) {
        Session session = HibernateUtil.getSession();
        return (Entity) session.createQuery("select e from " + getEntityClass().getSimpleName() + " e where id =" + id).uniqueResult();
    }

	@SuppressWarnings("unchecked")
	public List<Entity> list() {
		Session session = HibernateUtil.getSession();
		return session.createCriteria( getEntityClass()).list();
	}
	
	public void create( Entity entity ) {
		Session session = HibernateUtil.getSession();
		session.save( entity );
	}
	
	public void update( Entity entity ) {
		create( entity );
	} 
	
	public void delete( Integer id ) {
		Session session = HibernateUtil.getSession();
		session.createQuery("DELETE FROM " + getEntityClass().getSimpleName() + " WHERE ID = " + id ).executeUpdate();
	}
	
	public void delete( Entity entity ) {
		delete( entity.getId() );
	}
}
