package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class ClientesService {
	
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger( ClientesService.class.getName());

	public void salvarClientes( ClientesDTO clienteDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Clientes cliente = CLIENTES_DAO.parseFrom( clienteDTO );
		
		try {
			Clientes clienteRes = CLIENTES_DAO.search( 1 );
			if( clienteRes == null ) {	
				CLIENTES_DAO.create( cliente );
			} else {
				cliente.setId( clienteRes.getId());
				CLIENTES_DAO.update( cliente );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		clienteDTO.setId( cliente.getId() );
	}
}
