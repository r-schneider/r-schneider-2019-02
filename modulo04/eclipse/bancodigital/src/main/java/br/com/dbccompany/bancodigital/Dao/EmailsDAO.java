package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsDAO extends AbstractDAO<Emails> {
	
	ClientesDAO dao = new ClientesDAO();
	
	public Emails parseFrom( EmailsDTO dto ) {
		Emails email = null;
		if( dto.getId() != null ) {
			email = search( dto.getId());
		} else {
			email = new Emails();
		}
		email.setEmail( dto.getEmail() );
		email.setCliente( dao.parseFrom(dto.getCliente()) );
		
		return email;
	}
	
	@Override
	protected Class<Emails> getEntityClass() {
		return Emails.class;
	}
}
