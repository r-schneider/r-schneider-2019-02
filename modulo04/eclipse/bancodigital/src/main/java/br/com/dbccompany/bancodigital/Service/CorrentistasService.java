package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class CorrentistasService {
	
	private static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO();
	private static final Logger LOG = Logger.getLogger( CorrentistasService.class.getName());

	public void salvarCorrentistas( CorrentistasDTO correntistaDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Correntistas correntista = CORRENTISTAS_DAO.parseFrom( correntistaDTO );
		
		try {
			Correntistas correntistaRes = CORRENTISTAS_DAO.search( 1 );
			if( correntistaRes == null ) {	
				CORRENTISTAS_DAO.create( correntista );
			} else {
				correntista.setId( correntistaRes.getId());
				CORRENTISTAS_DAO.update( correntista );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		correntistaDTO.setId( correntista.getId() );
	}
}
