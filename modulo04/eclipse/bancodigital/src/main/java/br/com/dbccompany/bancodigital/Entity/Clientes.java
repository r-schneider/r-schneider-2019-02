package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
public class Clientes extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column( name = "ID_CLIENTE", nullable = false)
	private Integer id;
	
	private String cpf, nome, rg, conjuge;
	
	@Column( name = "DATA_NASCIMENTO")
	private String dataNascimento;
	
	@Column( name = "ESTADO_CIVIL")
	@Enumerated( EnumType.STRING )
	private TipoEstadoCivil estadoCivil;
	
	@ManyToOne
	@JoinColumn( name = "FK_ID_ENDERECO" )
	private Enderecos endereco;
	
	@OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
	private List<Emails> emails = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Telefones> telefones = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Correntistas> correntistas = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}
	
	public TipoEstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	
	public void setEstadoCivil(TipoEstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	public List<Emails> getEmails() {
		return emails;
	}

	public void setEmails(List<Emails> emails) {
		this.emails = emails;
	}

	public List<Telefones> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefones> telefones) {
		this.telefones = telefones;
	}
}
	