package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosDAO extends AbstractDAO<Bairros> {
	
	CidadesDAO dao = new CidadesDAO();
	
	public Bairros parseFrom( BairrosDTO dto ) {
		Bairros bairro = null;
		if( dto.getId() != null ) {
			bairro = search( dto.getId());
		} else {
			bairro = new Bairros();
		}
		bairro.setNome( dto.getBairro() );
		bairro.setCidade( dao.parseFrom(dto.getCidade()) );
		
		return bairro;
	}
	
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}
}