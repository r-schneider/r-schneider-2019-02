package br.com.dbccompany.bancodigital.Dao;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class AgenciasDAO extends AbstractDAO<Agencias> {
	
	EnderecosDAO endDAO = new EnderecosDAO();
	BancosDAO bancoDAO = new BancosDAO();
	List<Correntistas> correntistas = new ArrayList<Correntistas>();
	
	
	public Agencias parseFrom( AgenciasDTO dto ) {
		Agencias agencia = null;
		
		CorrentistasDAO correntistaDAO = new CorrentistasDAO();
		
		if( dto.getId() != null ) {
			agencia = search( dto.getId());
		} else {
			agencia = new Agencias();
		}
		
		agencia.setNome( dto.getAgencia() );
		agencia.setCodigo( dto.getCodigo() );
		
		agencia.setEndereco( endDAO.parseFrom(dto.getEndereco()) );
		agencia.setBanco( bancoDAO.parseFrom(dto.getBanco()) );
		

		List<CorrentistasDTO> correntistasDTO = dto.getCorrentistas();
		List<Correntistas> correntistas = new ArrayList<>();
		for(int i = 0; i < correntistasDTO.size();i++)
		{
			CorrentistasDTO correntista = correntistasDTO.get(i);
			correntistas.add(correntistaDAO.parseFrom(correntista));
		}
		
		return agencia;
	}
	
	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}
}
