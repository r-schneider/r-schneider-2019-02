package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.PaisesDAO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class PaisesService {
	
	private static final PaisesDAO PAISES_DAO = new PaisesDAO();
	private static final Logger LOG = Logger.getLogger( PaisesService.class.getName());

	public void salvarPaises( PaisesDTO paisDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Paises pais = PAISES_DAO.parseFrom( paisDTO );
		
		try {
			Paises paisRes = PAISES_DAO.search( 1 );
			if( paisRes == null ) {	
				PAISES_DAO.create( pais );
			} else {
				pais.setId( paisRes.getId());
				PAISES_DAO.update( pais );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		paisDTO.setId( pais.getId() );
	}
}
