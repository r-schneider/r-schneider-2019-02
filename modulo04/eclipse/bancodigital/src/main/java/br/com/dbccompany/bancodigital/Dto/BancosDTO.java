package br.com.dbccompany.bancodigital.Dto;

public class BancosDTO {

		private Integer id;
		private Integer codigo;
		private String banco;
		
		public Integer getId() {
			return id;
		}
		
		public void setId(Integer id) {
			this.id = id;
		}
		
		public Integer getCodigo() {
			return codigo;
		}
		
		public void setCodigo(Integer codigo) {
			this.codigo = codigo;
		}
		
		public String getBanco() {
			return banco;
		}
		
		public void setBanco(String banco) {
			this.banco = banco;
		}		
}
