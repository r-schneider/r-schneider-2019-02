package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EnderecosDAO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EnderecosService {
	
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	private static final Logger LOG = Logger.getLogger( EnderecosService.class.getName());

	public void salvarEnderecos( EnderecosDTO enderecoDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Enderecos endereco = ENDERECOS_DAO.parseFrom( enderecoDTO );
		
		try {
			Enderecos enderecoRes = ENDERECOS_DAO.search( 1 );
			if( enderecoRes == null ) {	
				ENDERECOS_DAO.create( endereco );
			} else {
				endereco.setId( enderecoRes.getId());
				ENDERECOS_DAO.update( endereco );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}
		
		enderecoDTO.setId( endereco.getId() );
	}
}
