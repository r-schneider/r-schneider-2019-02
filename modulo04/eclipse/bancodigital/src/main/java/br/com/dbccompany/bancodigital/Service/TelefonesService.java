package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.TelefonesDAO;
import br.com.dbccompany.bancodigital.Dto.TelefonesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesService {
	
	private static final TelefonesDAO TELEFONES_DAO = new TelefonesDAO();
	private static final Logger LOG = Logger.getLogger( TelefonesService.class.getName());
	
	public void salvarTelefones( TelefonesDTO telefoneDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Telefones telefone = TELEFONES_DAO.parseFrom( telefoneDTO );
		
		try {
			Telefones telefoneRes = TELEFONES_DAO.search( 1 );
			if( telefoneRes == null ) {	
				TELEFONES_DAO.create( telefone );
			} else {
				telefone.setId( telefoneRes.getId());
				TELEFONES_DAO.update( telefone );
			}	
			
			if( started ) {
				transaction.commit();
			}
		} catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e);
		}	
		
		telefoneDTO.setId( telefone.getId() );
	}
}
