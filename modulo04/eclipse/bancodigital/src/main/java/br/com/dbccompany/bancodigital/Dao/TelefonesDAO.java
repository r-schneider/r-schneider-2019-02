package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.TelefonesDTO;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesDAO extends AbstractDAO<Telefones> {
	
	public Telefones parseFrom( TelefonesDTO dto ) {
		Telefones telefone = null;
		if( dto.getId() != null ) {
			telefone = search( dto.getId());
		} else {
			telefone = new Telefones();
		}
		telefone.setNumero( dto.getNumero() );
		
		return telefone;
	}
	
	@Override
	protected Class<Telefones> getEntityClass() {
		return Telefones.class;
	}
}
