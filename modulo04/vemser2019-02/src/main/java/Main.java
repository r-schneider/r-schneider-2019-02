import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main ( String[] args ) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement( "SELECT TNAME FROM TAB WHERE TNAME = 'PAISES'" )
                    .executeQuery();
            if ( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE PAISES(\n"
                        + "ID_PAIS INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + " )" ).execute();
            }

            ResultSet exists = conn.prepareStatement( "SELECT NOME FROM PAISES WHERE NOME = 'Brasil'" ).executeQuery();
            if ( !exists.next()) {
                PreparedStatement pst = conn.prepareStatement(" INSERT INTO PAISES(ID_PAIS, NOME) " + "VALUES (PAISES_SEQ.NEXTVAL, ?)");
                pst.setString(1, "Brasil");
                pst.executeUpdate();
            }else {
                System.out.println( String.format( "Pais existente na tabela" ));
            }

            rs = conn.prepareStatement( "SELECT * FROM PAISES" ).executeQuery();
            while ( rs.next() ) {
                System.out.println( String.format( "Nome do Pais: %s", rs.getString( "NOME" )));
            }

        }catch (SQLException ex) {
            Logger.getLogger( Connector.class.getName() ).log( Level.SEVERE, "ERRO AO CONSULTAR MAIN", ex );
        }

        try {
            ResultSet rs = conn.prepareStatement( "SELECT TNAME FROM TAB WHERE TNAME = 'ESTADOS'" )
                    .executeQuery();
            if ( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE ESTADOS(\n"
                        + "ID_ESTADO INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL, \n"
                        + "FK_ID_PAIS INTEGER NOT NULL \n"
                        + " )" ).execute();
            }
            ResultSet exists = conn.prepareStatement( "SELECT NOME FROM ESTADOS WHERE NOME = 'Rio Grande do Sul'" ).executeQuery();

            if ( !exists.next()) {
                int id_pais = 0;
                ResultSet fk = conn.prepareStatement("SELECT ID_PAIS FROM PAISES WHERE NOME = 'Brasil'").executeQuery();
                if( fk.next() ) {
                    id_pais = fk.getInt( "ID_PAIS" );
                }
                PreparedStatement pst = conn.prepareStatement(" INSERT INTO ESTADOS(ID_ESTADO, NOME, FK_ID_PAIS) " + "VALUES (ESTADOS_SEQ.NEXTVAL, ?, ?)");
                pst.setString(1, "Rio Grande do Sul");
                pst.setInt(2, id_pais);
                pst.executeUpdate();
            }else {
                System.out.println( String.format( "Estado existente na tabela" ));
            }

            rs = conn.prepareStatement( "SELECT * FROM ESTADOS" ).executeQuery();
            while ( rs.next() ) {
                System.out.println( String.format( "Nome do Estado: %s", rs.getString( "NOME" )));
            }

        }catch (SQLException ex) {
            Logger.getLogger( Connector.class.getName() ).log( Level.SEVERE, "ERRO AO CONSULTAR MAIN", ex );
        }

        try {
            ResultSet rs = conn.prepareStatement( "SELECT TNAME FROM TAB WHERE TNAME = 'CIDADES'" )
                    .executeQuery();
            if ( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE CIDADES(\n"
                        + "ID_CIDADE INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL, \n"
                        + "FK_ID_ESTADO INTEGER NOT NULL \n"
                        + " )" ).execute();
            }
            ResultSet exists = conn.prepareStatement( "SELECT NOME FROM CIDADES WHERE NOME = 'Canoas'" ).executeQuery();

            if ( !exists.next()) {
                int id_estado = 0;
                ResultSet fk = conn.prepareStatement("SELECT ID_ESTADO FROM ESTADOS WHERE NOME = 'Rio Grande do Sul'").executeQuery();
                if( fk.next() ) {
                    id_estado = fk.getInt( "ID_ESTADO" );
                }
                PreparedStatement pst = conn.prepareStatement(" INSERT INTO CIDADES(ID_CIDADE, NOME, FK_ID_ESTADO) " + "VALUES (CIDADES_SEQ.NEXTVAL, ?, ?)");
                pst.setString(1, "Canoas");
                pst.setInt(2, id_estado);
                pst.executeUpdate();
            }else {
                System.out.println( String.format( "Cidade existente na tabela" ));
            }

            rs = conn.prepareStatement( "SELECT * FROM CIDADES" ).executeQuery();
            while ( rs.next() ) {
                System.out.println( String.format( "Nome da Cidade: %s", rs.getString( "NOME" )));
            }

        }catch (SQLException ex) {
            Logger.getLogger( Connector.class.getName() ).log( Level.SEVERE, "ERRO AO CONSULTAR MAIN", ex );
        }

        try {
            ResultSet rs = conn.prepareStatement( "SELECT TNAME FROM TAB WHERE TNAME = 'BAIRROS'" )
                    .executeQuery();
            if ( !rs.next() ) {
                conn.prepareStatement( "CREATE TABLE BAIRROS(\n"
                        + "ID_BAIRRO INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL, \n"
                        + "FK_ID_CIDADE INTEGER NOT NULL \n"
                        + " )" ).execute();
            }
            ResultSet exists = conn.prepareStatement( "SELECT NOME FROM BAIRROS WHERE NOME = 'Centro'" ).executeQuery();

            if ( !exists.next()) {
                int id_cidade = 0;
                ResultSet fk = conn.prepareStatement("SELECT ID_CIDADE FROM CIDADES WHERE NOME = 'Canoas'").executeQuery();
                if( fk.next() ) {
                    id_cidade = fk.getInt( "ID_CIDADE" );
                }
                PreparedStatement pst = conn.prepareStatement(" INSERT INTO BAIRROS(ID_BAIRRO, NOME, FK_ID_CIDADE) " + "VALUES (BAIRROS_SEQ.NEXTVAL, ?, ?)");
                pst.setString(1, "Centro");
                pst.setInt(2, id_cidade);
                pst.executeUpdate();
            }else {
                System.out.println( String.format( "Bairro ja existente na tabela" ));
            }

            rs = conn.prepareStatement( "SELECT * FROM BAIRROS" ).executeQuery();
            while ( rs.next() ) {
                System.out.println( String.format( "Nome do Bairro: %s", rs.getString( "NOME" )));
            }

        }catch (SQLException ex) {
            Logger.getLogger( Connector.class.getName() ).log( Level.SEVERE, "ERRO AO CONSULTAR MAIN", ex );
        }

    }
}
